const Route = require('../core/route');
const Types = require('../core/types');
const Db = require('../model');
const sequelize = require('sequelize');
const moment = require('moment');
const xlsx = require('node-xlsx');
const Request = require('../core/request');


Route.post({
    url: '/export/logunit',
    summary: 'Xuất Excel logunit',
    requireAuth: true,
    parameter: {
        start_date: {required: true, type: Types.datetime({description: 'thời gian bắt đầu'})},
        end_date: {required: true, type: Types.datetime({description: 'thời gian kết thúc'})},
    },
    response: Types.file(),
    handle: async (control, route) => {
        let filter = {
            // id_unit:route.args.id_unit,
            search: route.args.search,
        };
        if (route.args.start_date && route.args.end_date){
            filter.datetime = {
                gte: route.args.start_date,
                lte: route.args.end_date,
            };
        }
        
        let data = await Db.log_unit.list(1, 20000, filter);

        // RESULT
        let result = data.rows.map(x => {
            return {
                id: x.id,
                datetime: x.datetime,
                id_unit: x.id_string,
                numbat: x.battunit.numbat,
                ampe: x.ampe,
                vol: x.vol,
                time_discharge: x.time_discharge,
                id_discharge: x.id_discharge,
                momh: x.momh,            
                dodavg: x.dodavg,
                level: x.level,
                temp: x.temp,
            }
        });
        
        let list = [['ID Log', 'Time', 'ID Unit', 'Numbat Unit', 'Ampe', 'Vol','Time Discharge','Id Discharge','mOmh','dodavg','Level','Temp']];
        for (let item of result) {
            let arr = [];
            arr.push(item.id),
            arr.push(item.datetime),
            arr.push(item.id_unit),
            arr.push(item.numbat),
            arr.push(item.ampe),
            arr.push(item.vol),
            arr.push(item.time_discharge),
            arr.push(item.id_discharge),
            arr.push(item.momh),
            arr.push(item.dodavg),
            arr.push(item.level),
            arr.push(item.temp),
            list.push(arr)
        };
        let buffer = xlsx.build([{name: "mySheetName", data: list}]);
        let fileName = 'ListLogUnit.xlsx';

        control.setHeader('Content-Disposition', `attachment; filename=${fileName}`);
        control.setHeader('Content-Length', buffer.byteLength);
        return buffer;
    },
    logHandle: async (control, route, result, isError) => {
        let parameter = route.args;
        
        await control.writeLogAction({
            user_id: route.session.accountId ? route.session.accountId : null,
            parameter:parameter,
            response: result,
            isError: isError,
        })
    }
});

Route.post({
    url: '/export/logenddischarge',
    summary: 'Xuất Excel logenddischarge',
    requireAuth: true,
    parameter: {
        start_date: {required: true, type: Types.datetime({description: 'thời gian bắt đầu'})},
        end_date: {required: true, type: Types.datetime({description: 'thời gian kết thúc'})},
    },
    response: Types.file(),
    handle: async (control, route) => {
        let filter = {
            // id_unit:route.args.id_unit,
            search: route.args.search,
        };
        if (route.args.start_date && route.args.end_date){
            filter.datetime = {
                gte: route.args.start_date,
                lte: route.args.end_date,
            };
        }
        
        let data = await Db.logenddischarge.list(1, 20000, filter);

        // RESULT
        let result = data.rows.map(x => {
            return {
                id: x.id,
                datetime: x.datetime,
                id_unit: x.id_string,
                numbat: x.battunit.numbat,
                ampe: x.ampe,
                vol: x.vol,
                time_discharge: x.time_discharge, 
                dod: x.dod,
            }
        });
        
        let list = [['ID Log', 'Time', 'ID Unit', 'Numbat Unit', 'Ampe', 'Vol','Time Discharge','Dod']];
        for (let item of result) {
            let arr = [];
            arr.push(item.id),
            arr.push(item.datetime),
            arr.push(item.id_unit),
            arr.push(item.numbat),
            arr.push(item.ampe),
            arr.push(item.vol),
            arr.push(item.time_discharge),
            arr.push(item.dod)
            list.push(arr)
        };
        let buffer = xlsx.build([{name: "mySheetName", data: list}]);
        let fileName = 'ListLogEndDischarge.xlsx';

        control.setHeader('Content-Disposition', `attachment; filename=${fileName}`);
        control.setHeader('Content-Length', buffer.byteLength);
        return buffer;
    },
    logHandle: async (control, route, result, isError) => {
        let parameter = route.args;
        
        await control.writeLogAction({
            user_id: route.session.accountId ? route.session.accountId : null,
            parameter:parameter,
            response: result,
            isError: isError,
        })
    }
});

Route.get({
    url: '/battunitlist',
    summary: 'Lấy danh sách bình',
    // requireAuth: true,
    // permission: ['read_list_account'],
    paging: true,
    parameter: {
        id_string: {
            type:Types.string({description: 'từ khóa tìm kiếm theo tên hoặc ID' }),
            required: true
        },
        search: Types.string({description: 'từ khóa tìm kiếm theo tên hoặc ID'}),
    },
    response: Types.list(Types.object({
        id: Types.integer({description: 'ID'}),
        datetime: Types.datetime({description: 'ngày tạo'}),
        serial: Types.string({description: 'ID'}),
        numbat: Types.integer({description: 'ID'}),
        ip_adr: Types.string(),
        info:Types.object({
                datetime: Types.datetime({description: 'ngày tạo'}),
                vol: Types.number({description: 'tên tổ'}),
                ampe: Types.number({description: 'tên tổ'}),
                time_discharge: Types.number({description: 'vị trí'}),
                momh: Types.number({description: 'tên tổ'}),
                dodavg: Types.number({description: 'tên tổ'}),
                level: Types.number({description: 'tên tổ'}),
                alarm: Types.number(),
                alarm_id: Types.number(),
            }),
    })),
    handle: async (control, route) => {
        let filter = {
            id_string:route.args.id_string,
            search: route.args.search,
        };

        let data = await Db.battunit.listwithlast(route.args.page, route.args.page_size, filter);

        control.setPaging(data.count);
        return data.rows.map(x => {
            let info = null;
            if(x.current_value.length > 0){
                info = {
                    datetime: x.current_value[0].datetime,
                    vol: x.current_value[0].vol,
                    ampe: x.current_value[0].ampe,
                    time_discharge: x.current_value[0].time_discharge,
                    momh: x.current_value[0].momh,
                    dodavg: x.current_value[0].dodavg,
                    level: x.current_value[0].level,
                    alarm: x.alarm[0] != undefined ? x.alarm[0].ischeck : 0,
                    alarm_id: x.alarm[0] != undefined ? x.alarm[0].id : 0
                };
            }
            else{
                info = {
                    datetime: moment(),
                    vol: 0,
                    ampe: 0,
                    time_discharge: 0,
                    momh: 0,
                    dodavg: 0,
                    level: 0,
                    alarm: 0,
                    alarm_id:0
                };
            }
            return {
                id: x.id,
                datetime: x.datetime,
                serial: x.serial,
                numbat: x.numbat,
                ip_adr: x.ip_adr,
                info:info,
            }
        });
    }
});

Route.get({
    url: '/log_battunit',
    summary: 'Lấy danh sách bình',
    // requireAuth: true,
    // permission: ['read_list_account'],
    paging: true,
    parameter: {
    	id_unit: {
    		type:Types.string({description: 'từ khóa tìm kiếm theo tên hoặc ID'	}),
    		required: true
    	},
        search: Types.string({description: 'từ khóa tìm kiếm theo tên hoặc ID'}),
        start_date: { type: Types.datetime({description: 'thời gian bắt đầu'})},
        end_date: {type: Types.datetime({description: 'thời gian kết thúc'})},
    },
    response: Types.list(Types.object({
        id: Types.integer({description: 'ID'}),
        datetime: Types.datetime({description: 'ngày tạo'}),
        id_unit: Types.integer({description: 'ID'}),
	    ampe: Types.number({description: 'ID'}),
	    vol: Types.number({description: 'ID'}),
	    time_discharge: Types.integer({description: 'ID'}),
	    id_discharge: Types.integer({description: 'ID'}),
	    momh: Types.number({description: 'ID'}),
	    capunit: Types.number({description: 'ID'}),
        
    })),
    handle: async (control, route) => {
        let filter = {
        	id_unit:route.args.id_unit,
            search: route.args.search,
        };

        if (route.args.start_date && route.args.end_date){
            filter.datetime = {
                gte: route.args.start_date,
                lte: route.args.end_date,
            };
        }

        let data = await Db.log_unit.list(route.args.page, route.args.page_size, filter);

        control.setPaging(data.count);
        return data.rows.map(x => ({
            id: x.id,
            datetime: x.datetime,
            id_unit: x.id_unit,
		    ampe: x.ampe,
		    vol: x.vol,
		    time_discharge: x.time_discharge,
		    id_discharge: x.id_discharge,
		    momh: x.momh,
		    capunit: x.capunit,
        }));
    }
});

Route.get({
    url: '/log_battunitofstring',
    summary: 'Lấy danh sách bình',
    // requireAuth: true,
    // permission: ['read_list_account'],
    paging: true,
    parameter: {

        id_string: {
            type:Types.string({description: 'từ khóa tìm kiếm theo tên hoặc ID' }),
            required: true
        },
        search: Types.string({description: 'từ khóa tìm kiếm theo tên hoặc ID'}),
    },
    response: Types.list(Types.object({
        id: Types.integer({description: 'ID'}),
        datetime: Types.datetime({description: 'ngày tạo'}),
        id_unit: Types.integer({description: 'ID'}),
        ampe: Types.number({description: 'ID'}),
        vol: Types.number({description: 'ID'}),
        time_discharge: Types.integer({description: 'ID'}),
        id_discharge: Types.integer({description: 'ID'}),
        momh: Types.number({description: 'ID'}),
        level: Types.number({description: 'ID'}),
        dodavg: Types.number({description: 'ID'}),
        temp: Types.number({description: 'ID'}),
        battunit: Types.object({
            numbat:Types.integer(),
            serial:Types.string(),
        }),
    })),
    handle: async (control, route) => {
        
        let dataString = await Db.battunit.listidunit(route.args.page, route.args.page_size, route.args.id_string);
        
        let listId = [];
        for(let item of dataString.rows){
            listId.push(item.id);
        }
        let filter = {
            listid: listId,
            search: route.args.search,
        };

        let data = await Db.log_unit.list(route.args.page, route.args.page_size, filter);

        control.setPaging(data.count);
        return data.rows.map(x => ({
            id: x.id,
            datetime: x.datetime,
            id_unit: x.id_unit,
            ampe: x.ampe ? x.ampe : 0,
            vol: x.vol ? x.vol : 0,
            time_discharge: x.time_discharge ? x.time_discharge : 0,
            id_discharge: x.id_discharge,
            momh: x.momh ? x.momh : 0,
            level: x.level ? x.level : 0,
            dodavg: x.dodavg ? x.dodavg : 0,
            temp : x.temp ? x.temp : 0,
            battunit:{
                numbat: x.battunit ? x.battunit.numbat : null,
                serial: x.battunit ? x.battunit.serial : null,
            }
        }));
    }
});

Route.get({
    url: '/log_battunit_50',
    summary: 'Lấy log bình mới nhất',
    // requireAuth: true,
    // permission: ['read_list_account'],
    paging: true,
    parameter: {
    	id_unit: {
    		type:Types.string({description: 'từ khóa tìm kiếm theo tên hoặc ID'	}),
    		required: true
    	},
        search: Types.string({description: 'từ khóa tìm kiếm theo tên hoặc ID'}),
    },
    response: Types.list(Types.object({
        id: Types.integer({description: 'ID'}),
        datetime: Types.datetime({description: 'ngày tạo'}),
        id_unit: Types.integer({description: 'ID'}),
	    ampe: Types.number({description: 'ID'}),
	    vol: Types.number({description: 'ID'}),
        temp: Types.number({description: 'ID'}),
	    time_discharge: Types.integer({description: 'ID'}),
	    id_discharge: Types.integer({description: 'ID'}),
	    momh: Types.number({description: 'ID'}),
	    capunit: Types.number({description: 'ID'}),
        
    })),
    handle: async (control, route) => {
        let filter = {
        	id_unit:route.args.id_unit,
            search: route.args.search,
        };

        let data = await Db.log_unit.list(1, 25, filter);

        control.setPaging(data.count);
        return data.rows.map(x => ({
            id: x.id,
            datetime: x.datetime,
            id_unit: x.id_unit,
		    ampe: x.ampe,
		    vol: x.vol,
            temp: x.temp,
		    time_discharge: x.time_discharge,
		    id_discharge: x.id_discharge,
		    momh: x.momh,
		    capunit: x.capunit,
        }));
    }
});

Route.get({
    url: '/history/unit',
    summary: 'Lấy thống kế unit',
    parameter: {
        start_date: { type: Types.datetime({description: 'thời gian bắt đầu'})},
        end_date: {type: Types.datetime({description: 'thời gian kết thúc'})},
        typeview: Types.string({description: 'loại phân loại theo thời gian'}),
        id_unit:Types.integer({description: 'id unit'}),
    },
    response: Types.object({
        // list_date: Types.list(Types.datetime({description: 'Ngày thống kê'})),
        chart_date: Types.list(Types.object({
            date: Types.string({description: 'tên cs'}),
            avgvol: Types.number({description: 'tổng ticket'}),
            avgampe: Types.number({description: 'tổng ticket'}),
            avgomh: Types.number({description: 'tổng ticket'}),
        }),{description: 'Dữ liệu thống kê theo cs'}),
    }),
    handle: async (control, route) => {
        let filter = {};
        if (route.args.start_date && route.args.end_date){
            filter.datetime = {
                gte: route.args.start_date,
                lte: route.args.end_date,
            };
        }
        if (route.args.id_unit){
            filter.id_unit = route.args.id_unit;
        }
        let groupArr = [];
        let attributeArr = [];
        if(route.args.typeview && route.args.typeview == 'Time'){
            groupArr = ['day','hour','month'];
            attributeArr = [
                [ sequelize.fn('day', sequelize.col('datetime')), 'day'],
                [ sequelize.fn('hour', sequelize.col('datetime')), 'hour'],
                [ sequelize.fn('month', sequelize.col('datetime')), 'month'],
                [ sequelize.fn('AVG', sequelize.col('vol')),'avgvol'],
                [ sequelize.fn('AVG', sequelize.col('ampe')),'avgampe'],
                [ sequelize.fn('AVG', sequelize.col('momh')),'avgomh'],
            ];
        }
        else{
            
            groupArr = ['date'];
            attributeArr = [
                [ sequelize.fn(`date`, sequelize.col('datetime')), 'date'],
                [ sequelize.fn('AVG', sequelize.col('vol')),'avgvol'],
                [ sequelize.fn('AVG', sequelize.col('ampe')),'avgampe'],
                [ sequelize.fn('AVG', sequelize.col('momh')),'avgomh'],
            ];
        }
        let groupUnit = {
            where: filter,
            attributes: attributeArr,
            group: groupArr,
            order: [['datetime', 'asc']],
        }
        let chartData = await Db.log_unit.listGroupAll(groupUnit);
        let datechart = await Promise.all(chartData.map(async x => {
            let label = '';

            if(x.dataValues.date){
                label = x.dataValues.date;
            }
            else{
                label = x.dataValues.day + "/" +  x.dataValues.month + ' : ' + x.dataValues.hour + "h";
            }
            return {
                date: label,
                avgvol: x.dataValues.avgvol ? x.dataValues.avgvol : 0,
                avgampe: x.dataValues.avgampe ? x.dataValues.avgampe : 0,
                avgomh: x.dataValues.avgomh ? x.dataValues.avgomh : 0,
            }
        }));

        // let listDate = [];
        // let listDateRs = []
        // for (var i in datechart) {
        //     if(listDate.indexOf(datechart[i].date) == -1 ){
        //         listDate.push(datechart[i].date);
        //         listDateRs.push(moment(datechart[i].date));
        //     }
        // }

        return {
            // list_date:listDateRs,
            chart_date: datechart
        }
    }
});

Route.get({
    url: '/history/unit_enddischarge',
    summary: 'Lấy thống kế unit_enddischarge',
    parameter: {
        start_date: { type: Types.datetime({description: 'thời gian bắt đầu'})},
        end_date: {type: Types.datetime({description: 'thời gian kết thúc'})},
        typeview: Types.string({description: 'loại phân loại theo thời gian'}),
        id_unit:Types.integer({description: 'id unit'}),
    },
    response: Types.object({
        // list_date: Types.list(Types.datetime({description: 'Ngày thống kê'})),
        chart_date: Types.list(Types.object({
            date: Types.string({description: 'tên cs'}),
            avgvol: Types.number({description: 'tổng ticket'}),
            avgampe: Types.number({description: 'tổng ticket'}),
            avgdod: Types.number({description: 'tổng ticket'}),
        }),{description: 'Dữ liệu thống kê theo cs'}),
    }),
    handle: async (control, route) => {
        let filter = {};
        if (route.args.start_date && route.args.end_date){
            filter.datetime = {
                gte: route.args.start_date,
                lte: route.args.end_date,
            };
        }
        if (route.args.id_unit){
            filter.id_unit = route.args.id_unit;
        }
        let groupArr = [];
        let attributeArr = [];
        if(route.args.typeview && route.args.typeview == 'Time'){
            groupArr = ['day','hour','month'];
            attributeArr = [
                [ sequelize.fn('day', sequelize.col('datetime')), 'day'],
                [ sequelize.fn('hour', sequelize.col('datetime')), 'hour'],
                [ sequelize.fn('month', sequelize.col('datetime')), 'month'],
                [ sequelize.fn('AVG', sequelize.col('vol')),'avgvol'],
                [ sequelize.fn('AVG', sequelize.col('ampe')),'avgampe'],
                [ sequelize.fn('AVG', sequelize.col('dod')),'avgdod'],
            ];
        }
        else{
            
            groupArr = ['date'];
            attributeArr = [
                [ sequelize.fn(`date`, sequelize.col('datetime')), 'date'],
                [ sequelize.fn('AVG', sequelize.col('vol')),'avgvol'],
                [ sequelize.fn('AVG', sequelize.col('ampe')),'avgampe'],
                [ sequelize.fn('AVG', sequelize.col('dod')),'avgdod'],
            ];
        }
        let groupUnit = {
            where: filter,
            attributes: attributeArr,
            group: groupArr,
            order: [['datetime', 'asc']],
        }
        let chartData = await Db.logenddischarge.listGroupAll(groupUnit);
        let datechart = await Promise.all(chartData.map(async x => {
            let label = '';

            if(x.dataValues.date){
                label = x.dataValues.date;
            }
            else{
                label = x.dataValues.day + "/" +  x.dataValues.month + ' : ' + x.dataValues.hour + "h";
            }
            return {
                date: label,
                avgvol: x.dataValues.avgvol ? x.dataValues.avgvol : 0,
                avgampe: x.dataValues.avgampe ? x.dataValues.avgampe : 0,
                avgdod: x.dataValues.avgdod ? x.dataValues.avgdod : 0,
            }
        }));

        // let listDate = [];
        // let listDateRs = []
        // for (var i in datechart) {
        //     if(listDate.indexOf(datechart[i].date) == -1 ){
        //         listDate.push(datechart[i].date);
        //         listDateRs.push(moment(datechart[i].date));
        //     }
        // }

        return {
            // list_date:listDateRs,
            chart_date: datechart
        }
    }
});

Route.post({
    url: '/create/unit',
    summary: 'Tạo mới unit',
    requireAuth: true,
    parameter: {
        numbat: {
            required: true,
            type: Types.number()
        },
        id_string: {
            required: true,
            type: Types.number()
        },
        serial: {
            required: true,
            type: Types.string()
        },
        ip_adr:{
            required: true,
            type: Types.string()
        }
    },
     response: Types.object({
        id: Types.number(),
        numbat: Types.number(),
        id_string: Types.string(),
        serial: Types.string(),
        ip_adr:Types.string(),
    }),
    error: {
        not_found: 'không tìm thấy dữ liệu',
    },
    handle: async (control, route) => {
        let kq = await Db.battunit.create(route.args.id_string,route.args.numbat,route.args.serial,route.args.ip_adr);
        return {
            id:kq.dataValues.id,
            numbat: kq.numbat,
            id_string: kq.id_string,
            serial: kq.serial,
            ip_adr:kq.ip_adr
        };
    },
    logHandle: async (control, route, result, isError) => {
        let parameter = route.args;
        
        await control.writeLogAction({
            user_id: route.session.accountId ? route.session.accountId : null,
            parameter:parameter,
            response: result,
            isError: isError,
        })
    }
});

Route.post({
    url: '/update/unit',
    summary: 'Cập nhật unit',
    requireAuth: true,
    parameter: {
        id: {
            required: true,
            type: Types.number()
        },
        numbat: {
            required: true,
            type: Types.number()
        },
        id_string: {
            required: true,
            type: Types.number()
        },
        serial: {
            required: true,
            type: Types.string()
        },
        ip_adr:{
            required: true,
            type: Types.string()
        }
    },
     response: Types.object({
        id: Types.number(),
        numbat: Types.number(),
        id_string: Types.string(),
        serial: Types.string(),
        ip_adr:Types.string()
    }),
    error: {
        not_found: 'không tìm thấy dữ liệu',
    },
    handle: async (control, route) => {
        let kq = await Db.battunit.update(route.args.id,route.args.id_string,route.args.numbat,route.args.serial,route.args.ip_adr);
        return {
            id:kq.id,
            numbat: kq.numbat,
            id_string: kq.id_string,
            serial: kq.serial,
            ip_adr:kq.ip_adr
        };
    },
    logHandle: async (control, route, result, isError) => {
        let parameter = route.args;
        
        await control.writeLogAction({
            user_id: route.session.accountId ? route.session.accountId : null,
            parameter:parameter,
            response: result,
            isError: isError,
        })
    }
});

Route.get({
    url: '/log_analysis',
    summary: 'Lấy log bình mới nhất',
    // requireAuth: true,
    // permission: ['read_list_account'],
    paging: true,
    parameter: {
        id_unit: {
            type:Types.string({description: 'từ khóa tìm kiếm theo tên hoặc ID' }),
            required: true
        },
        search: Types.string({description: 'từ khóa tìm kiếm theo tên hoặc ID'}),
    },
    response: Types.list(Types.object({
        id: Types.integer({description: 'ID'}),
        datetime: Types.datetime({description: 'ngày tạo'}),
        id_unit: Types.integer({description: 'ID'}),
        capremain: Types.number({description: 'ID'}),
        time_backupremain: Types.number({description: 'ID'}),
        attenuation: Types.number({description: 'ID'}),        
    })),
    handle: async (control, route) => {
        let filter = {
            id_unit:route.args.id_unit,
            search: route.args.search,
        };

        let data = await Db.loganalysis.list(1, 50, filter);

        control.setPaging(data.count);
        return data.rows.map(x => ({
            id: x.id,
            datetime: x.datetime,
            id_unit: x.id_unit,
            capremain: x.capremain,
            time_backupremain: x.time_backupremain,
            attenuation: x.attenuation,        
        }));
    }
});

Route.get({
    url: '/balance',
    summary: 'Lấy log bình mới nhất',
    parameter: {
        vol_avg: {
            type:Types.number(),
            required: true
        }
    },
    response: Types.raw(),
    handle: async (control, route) => {
        let filter = {};

        let data = await Db.battunit.listwithlast(1, 40, filter);

        data.rows.map(async x => {
            let info = null;
            if(x.current_value.length > 0){
                if(x.current_value[0].vol > route.args.vol_avg)
                {
                    console.log(x.numbat)
                    console.log(x.current_value[0].vol)
                    let url = "http://localhost:5000/api/balance/" + x.ip_adr
                    let rs = await Request.get(url);
                }
            }
        });

        return "ok"
    }
});