const Route = require('../core/route');
const Types = require('../core/types');
const Db = require('../model');
const xlsx = require('node-xlsx');

Route.post({
    url: '/export/logstring',
    summary: 'Xuất Excel logstring',
    requireAuth: true,
    parameter: {
        start_date: {required: true, type: Types.datetime({description: 'thời gian bắt đầu'})},
        end_date: {required: true, type: Types.datetime({description: 'thời gian kết thúc'})},
    },
    response: Types.file(),
    handle: async (control, route) => {
        let filter = {
            // id_unit:route.args.id_unit,
            search: route.args.search,
        };
        if (route.args.start_date && route.args.end_date){
            filter.datetime = {
                gte: route.args.start_date,
                lte: route.args.end_date,
            };
        }
        
        let data = await Db.log_string.list(1, 20000, filter);

        // RESULT
        let result = data.rows.map(x => {
            return {
                id: x.id,
                datetime: x.datetime,
                id_string: x.id_string,
                name_string: x.battstring.name_accu,
                ampe: x.ampe,
                vol: x.vol,
                temp: x.temp,
                hum: x.hum,
            }
        });
        
        let list = [['ID Log', 'Time', 'ID String', 'Name String', 'Ampe', 'Vol', 'Temp', 'Hum']];
        for (let item of result) {
            let arr = [];
            arr.push(item.id),
            arr.push(item.datetime),
            arr.push(item.id_string),
            arr.push(item.name_string),
            arr.push(item.ampe),
            arr.push(item.vol),
            arr.push(item.temp),
            arr.push(item.hum),
            list.push(arr)
        };
        let buffer = xlsx.build([{name: "mySheetName", data: list}]);
        let fileName = 'ListLogString.xlsx';

        control.setHeader('Content-Disposition', `attachment; filename=${fileName}`);
        control.setHeader('Content-Length', buffer.byteLength);
        return buffer;
    },
    logHandle: async (control, route, result, isError) => {
        let parameter = route.args;
        
        await control.writeLogAction({
            user_id: route.session.accountId ? route.session.accountId : null,
            parameter:parameter,
            response: result,
            isError: isError,
        })
    }
});

Route.get({
    url: '/battstring',
    summary: 'Lấy danh sách tổ bình',
    // requireAuth: true,
    // permission: ['read_list_account'],
    paging: true,
    parameter: {
        search: Types.string({description: 'từ khóa tìm kiếm theo tên hoặc ID'}),
    },
    response: Types.list(Types.object({
        id: Types.integer({description: 'ID'}),
        name_accu: Types.string({description: 'tên tổ'}),
        datetime: Types.datetime({description: 'ngày tạo'}),
        location: Types.string({description: 'vị trí'}),
        ip_add: Types.string({description: 'địa chỉ ip'}),
        id_discharge: Types.integer({description: 'id xả'}),
        ccquality: Types.integer({description: 'id xả'}),
    })),
    handle: async (control, route) => {
        let filter = {
            search: route.args.search,
        };

        let data = await Db.battstring.list(route.args.page, route.args.page_size, filter);

        control.setPaging(data.count);
        return data.rows.map(x => ({
            id: x.id,
            name_accu: x.name_accu,
            datetime: x.datetime,
            location: x.location,
            ip_add: x.ip_add,
            id_discharge: x.id_discharge,
            ccquality:x.ccquality
        }));
    }
});

Route.get({
    url: '/battstringwithunit',
    summary: 'Lấy danh sách tổ bình và bình',
    // requireAuth: true,
    // permission: ['read_list_account'],
    paging: true,
    parameter: {
        search: Types.string({description: 'từ khóa tìm kiếm theo tên hoặc ID'}),
        string_id :Types.integer()
    },
    response: Types.list(Types.object({
        id: Types.integer({description: 'ID'}),
        name_accu: Types.string({description: 'tên tổ'}),
        datetime: Types.datetime({description: 'ngày tạo'}),
        location: Types.string({description: 'vị trí'}),
        ip_add: Types.string({description: 'địa chỉ ip'}),
        id_discharge: Types.integer({description: 'id xả'}),
        ccquality:Types.integer({description: 'id xả'}),
        listunit:Types.list(Types.object({
            id: Types.integer({description: 'ID'}),
            numbat: Types.integer({description: 'tên tổ'}),
            serial: Types.string({description: 'serial'}),
            location: Types.string({description: 'vị trí'}),
            info:Types.object({
                datetime: Types.datetime({description: 'ngày tạo'}),
                vol: Types.number({description: 'tên tổ'}),
                ampe: Types.number({description: 'tên tổ'}),
                temp: Types.number(),
                time_discharge: Types.number({description: 'vị trí'}),
                momh: Types.number({description: 'tên tổ'}),
                capunit: Types.number({description: 'tên tổ'}),
            })
        })),
        info:Types.object({
            datetime: Types.datetime({description: 'ngày tạo'}),
            vol: Types.number({description: 'tên tổ'}),
            ampe: Types.number({description: 'tên tổ'}),
            temp: Types.number({description: 'vị trí'}),
            hum: Types.number({description: 'tên tổ'}),
        })
    })),
    handle: async (control, route) => {
        let filter = {
            search: route.args.search,
            id:     route.args.string_id
        };
        
        let data = await Db.battstring.listwithunit(route.args.page, route.args.page_size, filter);
        
        control.setPaging(data.count);
        
        return data.rows.map(x => {
            let info = null;
            if(x.current_value.length > 0){
                info = {
                    datetime: x.current_value[0].datetime,
                    vol: x.current_value[0].vol,
                    ampe: x.current_value[0].ampe,
                    temp: x.current_value[0].temp,
                    hum: x.current_value[0].hum,
                }
            }
            let tempunit = [];
            x.listunit.sort(function(a, b) {
                return a.id - b.id;
            });
            for (let unit of x.listunit) {
                let infounit = null;
                if(unit.current_value.length > 0){
                    infounit = {
                        datetime: unit.current_value[0].datetime,
                        vol: unit.current_value[0].vol,
                        ampe: unit.current_value[0].ampe,
                        temp: unit.current_value[0].temp,
                        time_discharge: unit.current_value[0].time_discharge,
                        momh: unit.current_value[0].momh,
                        capunit: unit.current_value[0].capunit,
                    }
                }
                tempunit.push({
                    id: unit.id,
                    numbat: unit.numbat,
                    serial: unit.serial,
                    location: unit.location,
                    info: infounit
                });
            }
            return {
                id: x.id,
                name_accu: x.name_accu,
                datetime: x.datetime,
                location: x.location,
                ip_add: x.ip_add,
                id_discharge: x.id_discharge,
                ccquality:x.ccquality,
                listunit: tempunit,
                info: info
            }
        });
    }
});


Route.get({
    url: '/log_battstring',
    summary: 'Lấy danh sách log tổ',
    // requireAuth: true,
    // permission: ['read_list_account'],
    paging: true,
    parameter: {
        id_unit: {
            type:Types.string({description: 'từ khóa tìm kiếm theo tên hoặc ID' }),
            required: true
        },
        search: Types.string({description: 'từ khóa tìm kiếm theo tên hoặc ID'}),
        start_date: { type: Types.datetime({description: 'thời gian bắt đầu'})},
        end_date: {type: Types.datetime({description: 'thời gian kết thúc'})},
    },
    response: Types.list(Types.object({
        id: Types.integer({description: 'ID'}),
        datetime: Types.datetime({description: 'ngày tạo'}),
        id_string: Types.integer({description: 'ID'}),
        ampe: Types.number({description: 'ID'}),
        vol: Types.number({description: 'ID'}),
        temp: Types.integer({description: 'ID'}),
        hum: Types.integer({description: 'ID'}),
        
    })),
    handle: async (control, route) => {
        let filter = {
            id_unit:route.args.id_unit,
            search: route.args.search,
        };
        if (route.args.start_date && route.args.end_date){
            filter.datetime = {
                gte: route.args.start_date,
                lte: route.args.end_date,
            };
        }
        let data = await Db.log_string.list(route.args.page, route.args.page_size, filter);

        control.setPaging(data.count);
        return data.rows.map(x => ({
            id: x.id,
            datetime: x.datetime,
            id_string: x.id_string,
            ampe: x.ampe,
            vol: x.vol,
            temp: x.temp,
            hum: x.hum,
        }));
    }
});

Route.get({
    url: '/select/string',
    summary: 'Lấy danh sách tổ',
    paging: true,
    parameter: {
        search: Types.string({description: 'từ khóa tìm kiếm theo tên của select'})
    },
    response: Types.list(Types.object({
        id: Types.integer({description: 'ID select'}),
        name: Types.string({description: 'tên select'}),
    })),
    handle: async (control, route) => {

        let filter =  {};
        if (route.args.search)
            filter.search = route.args.search;

        let data = await Db.battstring.list(route.args.page, route.args.page_size, filter);
        control.setPaging(data.count);
        return data.rows.map(x => ({
            id: x.id,
            name: x.name_accu,
        }));
    }
});

Route.get({
    url: '/select/unit',
    summary: 'Lấy danh sách bình',
    paging: true,
    parameter: {
        search: Types.string({description: 'từ khóa tìm kiếm theo tên của select'}),
        id_string:{
            type:Types.string({description: 'từ khóa tìm kiếm theo tên hoặc ID' }),
        },
    },
    response: Types.list(Types.object({
        id: Types.integer({description: 'ID select'}),
        name: Types.string({description: 'tên select'}),
    })),
    handle: async (control, route) => {

        let filter = {
            id_string:route.args.id_string,
            search: route.args.search,
        };

        let data = await Db.battunit.listwithlast(route.args.page, 50, filter);

        control.setPaging(data.count);
        
        return data.rows.map(x => ({
            id: x.id,
            name: x.numbat,
        }));
    }
});

Route.post({
    url: '/create/string',
    summary: 'Tạo mới string',
    requireAuth: true,
    parameter: {
        name_accu: {
            required: true,
            type: Types.string()
        },
        location: {
            required: true,
            type: Types.string()
        },
        ip_add: {
            required: true,
            type: Types.string()
        },
        id_discharge: {
            type: Types.number()
        },
        ccquality: {
            type: Types.number()
        },
    },
     response: Types.object({
        id: Types.number(),
        name_accu: Types.string(),
        location: Types.string(),
        ip_add: Types.string(),
        ccquality: Types.number(),
    }),
    error: {
        not_found: 'không tìm thấy dữ liệu',
    },
    handle: async (control, route) => {
        let kq = await Db.battstring.create(route.args.name_accu,route.args.location,route.args.ip_add,route.args.id_discharge,route.args.ccquality);
        return {
            id:kq.dataValues.id,
            name_accu: kq.name_accu,
            location: kq.location,
            ip_add: kq.ip_add,
            ccquality : kq.ccquality
        };
    },
    logHandle: async (control, route, result, isError) => {
        let parameter = route.args;
        
        await control.writeLogAction({
            user_id: route.session.accountId ? route.session.accountId : null,
            parameter:parameter,
            response: result,
            isError: isError,
        })
    }
});

Route.post({
    url: '/update/string',
    summary: 'Cập nhật string',
    requireAuth: true,
    parameter: {
        id: {
            required: true,
            type: Types.number()
        },
        name_accu: {
            required: true,
            type: Types.string()
        },
        location: {
            required: true,
            type: Types.string()
        },
        ip_add: {
            required: true,
            type: Types.string()
        },
        ccquality: {
            required: true,
            type: Types.number()
        },
    },
     response: Types.object({
        id: Types.number(),
        name_accu: Types.string(),
        location: Types.string(),
        ip_add: Types.string(),
        ccquality:Types.number(),
    }),
    error: {
        not_found: 'không tìm thấy dữ liệu',
    },
    handle: async (control, route) => {
        let kq = await Db.battstring.update(route.args.id,route.args.name_accu,route.args.location,route.args.ip_add,route.args.ccquality);
        return {
            id:kq.id,
            name_accu: kq.name_accu,
            location: kq.location,
            ip_add: kq.ip_add,
            ccquality:kq.ccquality
        };
    },
    logHandle: async (control, route, result, isError) => {
        let parameter = route.args;
        
        await control.writeLogAction({
            user_id: route.session.accountId ? route.session.accountId : null,
            parameter:parameter,
            response: result,
            isError: isError,
        })
    }
});