const Route = require('../core/route');
const Types = require('../core/types');
const Config = require('../config');
const Db = require('../model');
const moment = require('moment');

Route.get({
    url: '/profile',
    summary: 'Lấy thông tin tài khoản',
    requireAuth: true,
    response: Types.object({
        id: Types.integer({description: 'ID user'}),
        login: Types.string({description: 'tên đăng nhập'}),
        name: Types.string({description: 'tên tài khoản'}),
        is_root: Types.boolean({description: 'tài khoản là Root'}),
        permission: Types.list(Types.string({}), {description: 'danh sách quyền'}),
        // permission: Types.list(Types.string({}), {description: 'danh sách quyền'}),
        password: Types.string({description: 'password sip name'}),
    }),
    handle: async (control, route) => {
        //console.log(route.session);
        return {
            id: route.session.accountId,
            login: route.session.accountLogin,
            name: route.session.accountName,
            permission: route.session.permission,
            // is_root: route.session.isRoot,            
            // password:md5(route.session.extention + ':' + code + ':' + route.session.password),
        };
    }
});

Route.get({
    url: '/loguser',
    summary: 'Lấy danh sách log user',
    // requireAuth: true,
    // permission: ['read_list_account'],
    paging: true,
    parameter: {
        search: Types.string({description: 'từ khóa tìm kiếm theo tên hoặc ID'}),
        start_date: { type: Types.datetime({description: 'thời gian bắt đầu'})},
        end_date: {type: Types.datetime({description: 'thời gian kết thúc'})},
    },
    response: Types.list(Types.object({
        id: Types.integer({description: 'ID'}),
        date: Types.datetime({description: 'ngày tạo'}),
        user_id:  Types.integer({description: 'id_unit'}),
        user_name:  Types.string({description: 'id_unit'}),
        method:  Types.string({description: 'id_string'}),
        url: Types.string({description: 'statealarm'}),
        parameter: Types.string({description: 'vol_unit'}),
        response: Types.string({description: 'resistor_unit'}),
        error:  Types.string({description: 'id_unit'}),
    })),
    handle: async (control, route) => {
        let filter = {
            search: route.args.search,
        };
        if (route.args.start_date && route.args.end_date){
            filter.datetime = {
                gte: route.args.start_date,
                lte: route.args.end_date,
            };
        }

        let data = await Db.log_action.list(route.args.page, route.args.page_size, filter);
        control.setPaging(data.count);
        return data.rows.map(x => ({
            id: x.id,
            date: moment(x.date).unix(),
            user_id:   x.user_id,
            user_name:   x.user.name,
            method:  x.method,
            url:  x.url,
            parameter:  x.parameter,
            response:x.response,
            error: x.error ? "Fail" : "Success"
        }));
    }
});