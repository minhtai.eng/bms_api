// const Route = require('../core/route');
// const Types = require('../core/types');
// const Elastic = require('../core/elastic');
// const DeliElastic = require('../core/deli-elastic');
// const Db = require('../model');
// const Util = require('../core/util');

// const Odoo = require('../core/odoo');
// const OdooDb = require('../core/odoo-db');
// const DeliDb = require('../core/deli-db');
// const moment = require('moment');
// const xlsx = require('node-xlsx');

// async function orderQuery(args) {
//     let merchantProducts = null;
//     if (args.merchant) {
//         let merchant = Elastic.init({
//             index: 'oc.shop',
//             filter_must: [
//                 {terms: {merchant: args.merchant.split(',')}},
//             ],
//             aggs: {
//                 data: {terms: {field: 'id'}},
//             }
//         });
//         merchant = await merchant.aggregate();
//         merchantProducts = merchant.data.buckets.map(x => x.key);
//     }

//     let orderUuid = null;
//     if (args.transaction) {
//         let transaction = Elastic.init({
//             index: 'oc.shop.transaction',
//             filter_must: [
//                 {term: {transaction: args.transaction}},
//             ],
//         });
//         transaction = await
//             transaction.searchFirst();
//         orderUuid = transaction.order_uuid;
//     }

//     let es = Elastic.init({
//         index: 'oc.shop.order',
//         size: 10000,
//         sort: [
//             {date_order: {order: 'desc'}},
//         ],
//         filter_must: [
//             {terms: {product_type: ['evoucher', 'combo']}},
//             {range: {date_order: {gte: args.start_date.format('YYYY-MM-DD HH:mm:ss')}}},
//             {range: {date_order: {lte: args.end_date.format('YYYY-MM-DD HH:mm:ss')}}},
//         ],
//     });

//     if (args.state)
//         es.filter_must.push({term: {state: args.state}});

//     if (args.payment_method)
//         es.filter_must.push({terms: {payment_method: args.payment_method.split(',')}});

//     if (args.payment_type)
//         es.filter_must.push({terms: {payment_type: args.payment_type.split(',')}});

//     if (args.order_code)
//         es.filter_must.push({term: {order_uuid: args.order_code}});

//     if (args.user_phone)
//         es.filter_must.push({term: {phone: args.user_phone}});

//     if (args.platform) {
//         for (let platform of args.platform.split(','))
//             es.filter_should.push({term: {platform: platform}});
//     }

//     if (args.product)
//         es.filter_must.push({terms: {product: args.product.split(',').map(x => x.trim().toLowerCase())}});
//     else if (merchantProducts)
//         es.filter_must.push({terms: {product: merchantProducts}});

//     if (orderUuid)
//         es.filter_must.push({term: {order_uuid: orderUuid}});

//     if (args.min_quantity)
//         es.filter_must.push({range: {quantity: {gte: args.min_quantity}}});

//     if (args.max_quantity)
//         es.filter_must.push({range: {quantity: {lte: args.max_quantity}}});

//     if (args.min_money)
//         es.filter_must.push({range: {discount_price: {gte: args.min_money}}});

//     if (args.max_money)
//         es.filter_must.push({range: {discount_price: {lte: args.max_money}}});

//     return es;
// }

// Route.post({
//     url: '/export/shop/order',
//     summary: 'Xuất Excel lịch sử bán hàng',
//     requireAuth: true,
//     permission: ['read_list_shop_order'],
//     parameter: {
//         start_date: {required: true, type: Types.datetime({description: 'thời gian bắt đầu'})},
//         end_date: {required: true, type: Types.datetime({description: 'thời gian kết thúc'})},
//         state: Types.string({
//             description: 'trạng thái hóa đơn',
//             enum: ['sale', 'complete', 'user_cancel', 'payment_expired'],
//         }),
//         payment_method: Types.string({
//             description: 'danh sách phương thức thanh toán',
//             check: (value) => {
//                 if (!value) return;
//                 if (value.split(',').some(x => !['unknown', 'at_store', 'bank', 'cc', 'lixi'].includes(x)))
//                     throw 'danh sách phương thức thanh toán không chính xác';
//             },
//         }),
//         payment_type: Types.string({
//             description: 'danh sách loại thanh toán',
//             check: (value) => {
//                 if (!value) return;
//                 if (value.split(',').some(x => !['cash', 'lixi'].includes(x)))
//                     throw 'danh sách loại thanh toán không chính xác';
//             },
//         }),
//         order_code: Types.string({description: 'mã hóa đơn'}),
//         user_phone: Types.string({description: 'số điện thoại'}),
//         platform: Types.string({description: 'nền tảng phát sinh giao dịch'}),
//         transaction: Types.string({description: 'mã thanh toán'}),
//         product: Types.string({description: 'danh sách ID sản phẩm'}),
//         merchant: Types.string({description: 'danh sách ID merchant'}),
//         min_quantity: Types.integer({description: 'số lượng mua tối thiểu'}),
//         max_quantity: Types.integer({description: 'số lượng mua tối đa'}),
//         min_money: Types.integer({description: 'tiền thanh toán tối thiểu'}),
//         max_money: Types.integer({description: 'tiền thanh toán tối đa'}),
//     },
//     response: Types.file(),
//     handle: async (control, route) => {
//         let es = await orderQuery(route.args);
//         let data = await es.search();

//         let userID = [];
//         for (let item of data) {
//             if (userID.indexOf(item.user_id) === -1) {
//                 userID.push(item.user_id)
//             }
//         }
//         let es_user = await Elastic.init({
//             index: 'res.user',
//             size: userID.length,
//             filter_must: [{terms: {id: userID}}]
//         });
//         let user = await es_user.search();

//         let prodID = [];
//         for (let item of data) {
//             if (prodID.indexOf(item.product) === -1) {
//                 prodID.push(item.product)
//             }
//         }
//         let es_prod = await Elastic.init({
//             index: 'oc.shop',
//             size: prodID.length,
//             filter_must: [{terms: {id: prodID}}]
//         });
//         let product = await es_prod.search();

//         let transactionID = [];
//         for (let item of data) {
//             if (transactionID.indexOf(item.order_uuid) === -1) {
//                 transactionID.push(item.order_uuid)
//             }
//         }
//         let es_trans = await Elastic.init({
//             index: 'oc.shop.transaction',
//             size: transactionID.length,
//             filter_must: [{terms: {order_uuid: transactionID}}]
//         });
//         let transaction = await es_trans.search();

//         let merhantID = [];
//         for (let item of product) {
//             if (merhantID.indexOf(item.merchant) === -1) {
//                 merhantID.push(item.merchant)
//             }
//         }
//         let es_merchant = await Elastic.init({
//             index: 'res.company',
//             filter_must: [{terms: {id: merhantID}}]
//         });
//         let merchant = await es_merchant.search();

//         // RESULT
//         let result = data.map(x => {

//             let user_result = user.filter(y => y.id === x.user_id)[0];

//             let product_result = product.filter(y => y.id === x.product)[0];

//             let transaction_result = transaction.filter(y => y.order_uuid === x.order_uuid)[0];

//             let merchant_result = !product_result ? null : merchant.filter(y => y.id === product_result.merchant)[0];

//             let transaction_state_result = !transaction_result ? null : transaction_result.transaction_state;

//             return {
//                 id: x.id,
//                 order_code: x.order_uuid,
//                 date: x.date_order,
//                 transaction_id: !transaction_result ? null : transaction_result.id,
//                 transaction_code: !transaction_result ? null : transaction_result.transaction,
//                 transaction_state: transaction_state_result === '1'? 'Thành công' : transaction_state_result,
//                 user_id: !user_result ? null : user_result.id,
//                 user_partner_id: !user_result ? null : user_result.partner_id,
//                 user_name: !user_result ? null : user_result.fullname,
//                 user_phone: x.phone,
//                 state: x.state,
//                 payment_method: x.payment_method,
//                 quantity: x.quantity,
//                 money: x.discount_price,
//                 cashback: !product_result ? null : product_result.cashback_price * x.quantity,
//                 product_id: !product_result ? null : product_result.id,
//                 product_name: !product_result ? null : product_result.name,
//                 merchant_id: !merchant_result ? null : merchant_result.id,
//                 merchant_name: !merchant_result ? null : merchant_result.name,
//                 platform: x.platform,
//             }
//         });

//         let list = [['ID đơn hàng', 'Thời gian', 'Mã đơn hàng', 'ID giao dịch', 'Mã giao dịch', 'Giao dịch thành công', 'ID của User', 'ID partner của user', 'tên User', 'Số điện thoại', 'Tình trạng xử lý', 'Phương thức', 'Số lượng', 'Tổng thanh toán', 'điểm lixi Cashback', 'Mã sản phẩm', 'Tên sản phẩm', 'mã Merchant', 'tên Merchant', 'Thiết bị']];
//         for (let item of result) {
//             let arr = [];
//             arr.push(item.id),
//                 arr.push(item.date),
//                 arr.push(item.order_code),
//                 arr.push(item.transaction_id),
//                 arr.push(item.transaction_code),
//                 arr.push(item.transaction_state),
//                 arr.push(item.user_id),
//                 arr.push(item.user_partner_id),
//                 arr.push(item.user_name),
//                 arr.push(item.user_phone),
//                 arr.push(item.state),
//                 arr.push(item.payment_method),
//                 arr.push(item.quantity),
//                 arr.push(item.money),
//                 arr.push(item.cashback),
//                 arr.push(item.product_id),
//                 arr.push(item.product_name),
//                 arr.push(item.merchant_id),
//                 arr.push(item.merchant_name),
//                 arr.push(item.platform),
//                 list.push(arr)
//         }
//         ;
//         let buffer = xlsx.build([{name: "mySheetName", data: list}]);
//         let fileName = 'ListOrder.xlsx';

//         control.setHeader('Content-Disposition', `attachment; filename=${fileName}`);
//         control.setHeader('Content-Length', buffer.byteLength);
//         return buffer;
//     }
// });

// Route.post({
//     url: '/export/shop/order/fail',
//     summary: 'xuất file excel danh sách các giao dịch voucher thất bại',
//     requireAuth: true,
//     permission: ['read_list_shop_order'],
//     parameter: {
//         start_date: {required: true, type: Types.datetime({description: 'thời gian bắt đầu'})},
//         end_date: {required: true, type: Types.datetime({description: 'thời gian kết thúc'})},
//         payment_method: Types.string({
//             description: 'phương thức thanh toán',
//             enum: ['unknown', 'at_store', 'bank', 'cc', 'lixi'],
//         }),
//         order_code: Types.string({description: 'mã hóa đơn'}),
//         user_phone: Types.string({description: 'số điện thoại'}),
//         platform: Types.string({description: 'nền tảng phát sinh giao dịch'}),
//         product: Types.string({description: 'danh sách ID sản phẩm'}),
//         merchant: Types.string({description: 'danh sách ID merchant'}),
//         min_quantity: Types.integer({description: 'số lượng mua tối thiểu'}),
//         max_quantity: Types.integer({description: 'số lượng mua tối đa'}),
//         state: {default: 'null', type: Types.string({description: 'trạng thái', enum: ['null', 'error', 'fixed']})},
//     },
//     response: Types.file(),
//     handle: async (control, route) => {
//         let sql = `
//             Select o.id as id, o.order_uuid as order_uuid, o.create_date as create_date, 
//             u.id as user_id, u.partner_id as user_partner_id, n.name as user_name,
//             o.phone as user_phone, o.state as state, o.payment_method as payment_method, o.quantity as quantity, o.discount_price as discount_price, 
//             p.cashback_price as cashback_price, p.id as product_id, p.name as product_name,
//             m.id as merchant_id, m.name as merchant_name, o.platform as platform, o.cs_state as cs_state
//             From oc_shop_order as o
//                 join res_users u on u.id = o.user
//                 join res_partner n on n.id = u.partner_id
//                 join oc_shop as p on p.id = o.product
//                 join res_company as m on m.id = p.merchant
//             Where o.create_date between :startDate and :endDate
//             `;

//         if (route.args.state === 'null')
//             sql += `and (o.cs_state is null or o.cs_state = 'error')
//                 and o.state in ('payment_expired', 'user_cancel')
//                 and not exists (
//                     Select *
//                     From oc_shop_order
//                     Where product = o.product and "user" = o."user"
//                         and create_date > o.create_date
//                 ) `;
//         else
//             sql += `and o.cs_state = :state `;
//         if (route.args.payment_method)
//             sql += 'and o.payment_method = :paymentMethod ';
//         if (route.args.order_code)
//             sql += 'and o.order_uuid = :orderCode ';
//         if (route.args.user_phone)
//             sql += 'and o.phone = :userPhone ';
//         if (route.args.platform)
//             sql += 'and o.platform = :platform ';
//         if (route.args.product)
//             sql += `and o.product in (${route.args.product}) `;
//         if (route.args.merchant)
//             sql += `and p.merchant in (${route.args.merchant}) `;
//         if (route.args.min_quantity)
//             sql += 'and o.quantity >= :minQuantity ';
//         if (route.args.max_quantity)
//             sql += 'and o.quantity <= :maxQuantity ';
//         sql += 'Order By o.create_date desc ';

//         let odooDb = OdooDb.init();
//         let data = await odooDb.query(sql, {
//             pageOffset: (route.args.page - 1) * route.args.page_size,
//             pageSize: route.args.page_size,
//             startDate: route.args.start_date.utc().format('YYYY-MM-DD HH:mm:ss'),
//             endDate: route.args.end_date.utc().format('YYYY-MM-DD HH:mm:ss'),
//             paymentMethod: route.args.payment_method,
//             orderCode: route.args.order_code,
//             userPhone: route.args.user_phone,
//             platform: route.args.platform,
//             merchant: route.args.merchant,
//             minQuantity: route.args.min_quantity,
//             maxQuantity: route.args.max_quantity,
//             state: route.args.state,
//         });

//         let result = data.map(x => {
//             return {
//                 id: x.id,
//                 order_code: x.order_uuid,
//                 date: moment(x.create_date).add(moment().utcOffset(), 'minutes').format('DD-MM-YYYY HH:mm'),
//                 user_id: x.user_id,
//                 user_partner_id: x.user_partner_id,
//                 user_name: x.user_name,
//                 user_phone: x.user_phone,
//                 state: x.state,
//                 payment_method: x.payment_method,
//                 quantity: x.quantity,
//                 money: x.discount_price,
//                 cashback: x.cashback_price * x.quantity,
//                 product_id: x.product_id,
//                 product_name: x.product_name,
//                 merchant_id: x.merchant_id,
//                 merchant_name: x.merchant_name,
//                 platform: x.platform,
//                 solution_state: x.cs_state,
//             }
//         });

//         let list = [['ID đơn hàng', 'Thời gian', 'Mã đơn hàng', 'ID của User', 'ID partner của user', 'tên User', 'Số điện thoại', 'Tình trạng xử lý', 'Phương thức', 'Số lượng', 'Tổng thanh toán', 'điểm lixi Cashback', 'Mã sản phẩm', 'Tên sản phẩm', 'mã Merchant', 'tên Merchant', 'Thiết bị', 'Tình trạng xử lý']];
//         for (let item of result) {
//             let arr = [];
//             arr.push(item.id),
//                 arr.push(item.date),
//                 arr.push(item.order_code),
//                 arr.push(item.user_id),
//                 arr.push(item.user_partner_id),
//                 arr.push(item.user_name),
//                 arr.push(item.user_phone),
//                 arr.push(item.state),
//                 arr.push(item.payment_method),
//                 arr.push(item.quantity),
//                 arr.push(item.money),
//                 arr.push(item.cashback),
//                 arr.push(item.product_id),
//                 arr.push(item.product_name),
//                 arr.push(item.merchant_id),
//                 arr.push(item.merchant_name),
//                 arr.push(item.platform),
//                 arr.push(item.solution_state),
//                 list.push(arr)
//         }

//         let buffer = xlsx.build([{name: "mySheetName", data: list}]);
//         let fileName = 'ListOrderFail.xlsx';

//         control.setHeader('Content-Disposition', `attachment; filename=${fileName}`);
//         control.setHeader('Content-Length', buffer.byteLength);
//         return buffer;
//     }
// });

// Route.post({
//     url: '/export/shop/voucher',
//     summary: 'Xuất Excel lịch sử mã voucher',
//     requireAuth: true,
//     permission: ['read_list_voucher'],
//     parameter: {
//         start_date: {required: true, type: Types.datetime({description: 'thời gian bắt đầu'})},
//         end_date: {required: true, type: Types.datetime({description: 'thời gian kết thúc'})},
//         state: Types.string({
//             description: 'trạng thái mã',
//             enum: ['selled', 'merchant_active'],
//         }),
//         merchant: Types.string({description: 'danh sách ID merchant'}),
//     },
//     response: Types.file(),
//     handle: async (control, route) => {
//         let list_store_id = [];
//         if (route.args.merchant) {
//             let store = await Elastic.init({
//                 index: 'stock.warehouse',
//                 size: 10000,
//                 filter_must: [
//                     {term: {merchant_id: route.args.merchant}},
//                 ],
//             }).search();

//             for (let item of store) {
//                 list_store_id.push(item.id);
//             }
//         }

//         let sql = `
//                 Select Cast(count(*) Over() as Integer) As total_count,
//                 code.id, code.pin, code.merchant_pin, code.serial, o.create_date As buy_date, code.use_date, code.store, osut.create_date as active_date, code.state, o.phone As phone,
//                 o.payment_type As payment_type, o.discount_price As discount_price,
//                 m.id As merchant_id, m.name As merchant_name,
//                 p.id As product_id, p.name As product_name, p.code_server, p.discount_price As unit_price, p.payment_discount_price As payment_unit_price, p.price As product_price,
//                 u.id As user_id, u.phone As user_phone, u.partner_id As user_partner_id,
//                 rp.name As user_name, sw.id As store_id, sw.name As store_name
                                
//                 From oc_evoucher_code As code
//                     Join oc_shop_order As o On o.id = code.evoucher_order
//                     Left Join oc_shop_user_transaction As osut On osut.evoucher = code.id
//                     Left Join stock_warehouse As sw On sw.id = osut.warehouse
//                     Join res_users As u On u.id = o.user
//                     Join res_partner As rp On rp.id = u.partner_id
//                     Join oc_shop As p On p.id = code.product
//                     Join res_company As m On m.id = p.merchant
//                 Where code.state in ('selled', 'merchant_active')
//                 `;
//         if (route.args.state && route.args.state !== 'selled')
//             sql += `and code.state = :state `;
//         if (route.args.merchant && route.args.state === 'merchant_active')
//             sql += `and code.store in (${list_store_id}) `;
//         else if (route.args.merchant)
//             sql += `and m.id in (${route.args.merchant}) `;

//         if (route.args.state && route.args.state === 'merchant_active')
//             sql += `and (code.use_date between :startDate and :endDate or osut.create_date between :startDate and :endDate) and code.state = :state`;
//         else
//             sql += `and o.create_date between :startDate and :endDate and code.state = :state`;

//         sql += ` Order by ${route.args.state && route.args.state === 'merchant_active' ? 'code.use_date' : 'o.create_date'} desc`;

//         let odooDb = OdooDb.init();
//         let data = await odooDb.query(sql, {
//             startDate: route.args.start_date.utc().format('YYYY-MM-DD HH:mm:ss'),
//             endDate: route.args.end_date.utc().format('YYYY-MM-DD HH:mm:ss'),
//             state: route.args.state,
//             merchant: route.args.merchant,
//         });

//         let result = await Promise.all(data.map(async x => {
//             let store = null;
//             if(x.store) {
//                 store = await Elastic.init({index: 'stock.warehouse', doctype: 'warehouse'}).get(x.store);
//             }

//             return {
//                 id: x.id,
//                 pin: x.code_server === 'lixi' ? x.pin : x.merchant_pin,
//                 serial: x.serial,
//                 buy_date: moment(x.buy_date).add(moment().utcOffset(), 'minutes'),
//                 active_date: x.state === 'merchant_active' ? x.use_date ? moment(x.use_date).add(moment().utcOffset(), 'minutes') : x.active_date ? moment(x.active_date).add(moment().utcOffset(), 'minutes') : null : null,
//                 state: x.state,
//                 merchant_id: x.merchant_id,
//                 merchant_name: x.merchant_name,
//                 store_id: store ? store.id :x.store_id,
//                 store_name: store ? store.name :x.store_name,
//                 lixi_price: x.payment_type === 'lixi'? x.discount_price : null,
//                 cash_price: x.payment_type === 'cash'? x.discount_price : null,
//                 unit_price: x.payment_type === 'lixi'? x.unit_price : x.payment_unit_price,
//                 product_id: x.product_id,
//                 product_name: x.product_name,
//                 user_phone: x.user_phone? x.user_phone : x.phone,
//                 price: x.product_price,
//             }
//         }));

//         let list = [['Thời gian mua', 'Thời gian sử dụng', 'Mã pin', 'Mã serial', 'Tên khách hàng', 'SDT khách hàng', 'ID sản phẩm', 'Tên sản phẩm','ID chi nhánh', 'Chi nhánh kích hoạt', 'ID merchant', 'Tên merchant', 'Trạng thái', 'Đơn giá', 'Giá trị thật']];
//         for (let item of result) {
//             let arr = [];
//             arr.push(item.buy_date.format('DD-MM-YYYY HH:mm'));
//             arr.push(item.state === 'merchant_active' ? item.active_date.format('DD-MM-YYYY HH:mm') : null);
//             arr.push(item.pin);
//             arr.push(item.serial);
//             arr.push(item.user_name);
//             arr.push(item.user_phone);
//             arr.push(item.product_id);
//             arr.push(item.product_name);
//             arr.push(item.store_id);
//             arr.push(item.store_name);
//             arr.push(item.merchant_id);
//             arr.push(item.merchant_name);
//             arr.push(item.state);
//             arr.push(item.unit_price);
//             arr.push(item.price);
//             list.push(arr);
//         }
//         let buffer = xlsx.build([{name: "mySheetName", data: list}]);
//         let fileName = 'ListVoucher.xlsx';

//         control.setHeader('Content-Disposition', `attachment; filename=${fileName}`);
//         control.setHeader('Content-Length', buffer.byteLength);
//         return buffer;
//     }
// });

// async function productCounter(productId, odooDb) {
//     let product = await Elastic.init({index: 'oc.shop', doctype: 'shop'}).get(productId);
//     let total_quantity, quantity_order, quantity_available, quantity_sell_success, quantity_sell_fail, quantity_pending;
    
//     let sql = `
//             Select p.id As product_id, p.name As product_name, p.quantity_limit As quantity_limit, SUM(oso.quantity) As quantity_pending,
//             `;
//     if (product.product_type === 'evoucher') {
//         sql+=`(select count(*) from oc_evoucher_code oec where oec.state in ('available', 'waiting') and 
//             p.id = oec.product ) as total_quantity,
//             (select count(*) from oc_evoucher_code oec where oec.state  in ('selled', 'merchant_active', 'coffeehouse_active') and 
//             p.id = oec.product ) as quantity_sell_success,
//             (select count(*) from oc_evoucher_code oec where oec.state  in ('deactive') and
//             p.id = oec.product ) as quantity_sell_fail
//         `;
//     }
//      else if (product.product_type === 'classic') {
//         quantity_available = product.quantity;
//         sql+=`(select SUM(oec.quantity) from oc_shop_order oec where oec.state  in ('complete') and 
//             p.id = oec.product) As quantity_sell_success
//         `;
//     } 
//     else if (product.product_type === 'combo') {
//         sql+=`(with t_combo_product as (select spi.pack, spi.product, sp.pname, spi.quantity, count(*) as evoucher_count
//         from oc_shop s
//         join oc_shop_pack_item spi on s.id = spi.product
//         join oc_evoucher_code ec on spi.product = ec.product
//         join oc_shop_pack sp on sp.id = spi.pack
//         where ec.state in ('available', 'waiting') and spi.quantity > 0 
//         group by 1, 2, 3, 4)
//         select min(t.evoucher_count / t.quantity) as quantity_available
//         from t_combo_product  t
//         join oc_shop s on s.pack = t.pack
//         where s.id = ${product.id}
//         group by t.pack, t.pname
//         order by t.pack),
//         `;
//         sql+=`(select count(*) from oc_evoucher_code oec
//             where oec.state  in ('available', 'waiting') and
//             pack.product = oec.product ) As total_quantity,
//             pack.quantity As pack_quantity,
//             (select SUM(oec.quantity) from oc_shop_order oec where oec.state = 'complete' and 
//             p.id = oec.product) As quantity_sell_success
//         `;
//     }
        
//     sql +=`                
//         From oc_shop As p
//             Left Join oc_shop_order As oso on oso.product = p.id
//             Left Join oc_shop_pack_item As pack on pack.pack = p.pack
//         Where p.id = ${product.id}
//         Group By product_id, pack.id
//     `;

//     if (!odooDb)
//         odooDb = OdooDb.init();
//     let data = await odooDb.query(sql);

//     total_quantity = 0;
//     quantity_available = 0;
//     quantity_sell_success = 0;
//     quantity_sell_fail = 0;
//     quantity_pending = data[0].quantity_pending || 0;
    
//     if(data.length != 0) {
//         if (product.product_type === 'evoucher') {
//             total_quantity = data[0].total_quantity || 0;
//             quantity_sell_success = data[0].quantity_sell_success || 0;
//             quantity_sell_fail = data[0].quantity_sell_fail || 0;
//             quantity_available = Math.min(total_quantity, product.quantity_limit);
//         }
//         else if (product.product_type === 'classic') {
//             quantity_available = product.quantity;
//             quantity_sell_success = data[0].quantity_sell_success || 0;
    
//         }
//         else if (product.product_type === 'combo') {
//             let pack_quantity = data[0].pack_quantity || 0
//             total_quantity = data[0].total_quantity || 0;
//             quantity_available = data[0].quantity_available || 0;

//             quantity_sell_success = data[0].quantity_sell_success || 0;
    
//         }
//     }
//     return {
//         totalQuantity: total_quantity,
//         quantityAvailable: quantity_available,
//         quantitySellSuccess: quantity_sell_success,
//         quantitySellFail: quantity_sell_fail,
//         quantityPending: quantity_pending,
//     }
// }

// Route.post({
//     url: '/export/shop/product',
//     summary: 'xuất danh sách sản phẩm',
//     // requireAuth: true,
//     // permission: ['read_list_product'],
//     parameter: {
//         search: Types.string({description: 'từ khóa tìm kiếm theo tên hoặc ID sản phẩm'}),
//         state: Types.string({description: 'trạng thái sản phẩm', enum: ['active', 'pause', 'inactive']}),
//         product_type: Types.string({description: 'trạng thái sản phẩm', enum: ['classic', 'evoucher', 'combo']}),
//         type: Types.string({description: 'danh sách loại sản phẩm (evoucher, classic, combo, combo_item)'}),
//         payment: Types.string({description: 'danh sách phương thức thanh toán cho phép (cash, lixi)'}),
//         merchant: Types.string({description: 'danh sách ID merchant'}),
//     },
//     response: Types.file(),
//     handle: async (control, route) => {
//         let odooDb = OdooDb.init();
//         let es = Elastic.init({
//             index: 'oc.shop',
//             size: 10000,
//             // sort: [
//             //     {name: {order: 'asc'}},
//             // ],
//             filter_must: [],
//         });
        
//         if (route.args.search) {
//             if (Number.isInteger(Number(route.args.search)))
//                 es.filter_should.push({term: {id: route.args.search}});
//             es.filter_should.push({match: {name: {query: route.args.search.toLowerCase(), operator: 'and'}}});
//         }
//         if (route.args.state) {
//             if (route.args.state === 'active') {
//                 es.filter_must.push({term: {state: 'active'}});
//                 es.filter_must.push({term: {allow_pay: true}});
//             }
//             else if (route.args.state === 'pause') {
//                 es.filter_must.push({term: {state: 'active'}});
//                 es.filter_must.push({term: {allow_pay: false}});
//             } else {
//                 es.filter_must.push({term: {state: route.args.state}});
//             }
//         }
//         if (route.args.type) {
//             let types = route.args.type.split(',').map(x => x.trim().toLowerCase());
//             if (types.includes('combo_item')) {
//                 let productInPack = Elastic.init({
//                     index: 'oc.shop.pack.item',
//                     aggs: {
//                         data: {terms: {field: 'product'}},
//                     }
//                 });
//                 productInPack = (await productInPack.aggregate()).data.buckets;
    
//                 if (productInPack.length > 0) {
//                     productInPack = productInPack.map(x => x.key);
//                     es.filter_must.push({terms: {id: productInPack}});
//                 }
//             }
    
//             types = types.map(x => x === 'combo_item' ? 'evoucher' : x);
//             es.filter_must.push({terms: {product_type: types}});
//         }
//         if (route.args.payment)
//             es.filter_must.push({terms: {payment_type: route.args.payment.split(',').map(x => x.trim().toLowerCase() === 'lixi' ? 1 : 2)}});
//         if (route.args.merchant)
//             es.filter_must.push({terms: {merchant: route.args.merchant.split(',').map(x => x.trim().toLowerCase())}});
//         if (route.args.product_type)
//             es.filter_must.push({term: {product_type: route.args.product_type}});
//         let data =  await es.search();
        
//         let result = await Promise.all(data.map(async x => {
//             // let counter = await productCounter(x.id);
//             let merchant = await Elastic.init({index: 'res.company', doctype: 'merchant'}).get(x.merchant);

//             let packInclude = Elastic.init({
//                 index: 'oc.shop.pack.item',
//                 filter_must: [
//                     {term: {product: x.id}},
//                 ],
//             });
//             packInclude = await packInclude.search();

//             let state;
//             if (x.state === 'active' && x.allow_pay === true)
//                 state = 'active';
//             else if (x.state === 'active' && x.allow_pay === false)
//                 state = 'pause';
//             else
//                 state = x.state;

//             let type = null;
//             if (x.product_type !== 'False')
//                 if (packInclude.length > 0)
//                     type = 'combo_item';
//                 else
//                     type = x.product_type;

//             let counter = await productCounter(x.id, odooDb);
            
//             return {
//                 id: x.id,
//                 name: x.name,
//                 state: state,
//                 type: type,
//                 payment: x.payment_type.length === 0 ? null : x.payment_type.map(y => y === 1 ? 'lixi' : 'cash'),
//                 merchant_id: merchant ? merchant.id : null,
//                 merchant_name: merchant ? merchant.name : null,
//                 quantity: x.quantity,
//                 quantity_limit: x.quantity_limit,
//                 discount_price: x.discount_price,
//                 price: x.price,
//                 total_quantity: counter.totalQuantity,
//                 quantity_available: counter.quantityAvailable,
//                 quantity_sell_success: counter.quantitySellSuccess,
//                 quantity_sell_fail: counter.quantitySellFail,
//                 quantity_pending: counter.quantityPending,
//             }
//         }));
//         let list = [['ID', 'Sản phẩm', 'Loại', 'Phương thức', 'Giá trị sản phẩm', 'Giá bán', 'ID merchant', 'Merchant', 'Trạng thái', 'Chỉ tiêu còn thiếu', 'Số lượng còn bán được', 'Số lượng đang chờ xử lý']];
//         for (let item of result) {
//             let arr = [];
//             arr.push(item.id);
//             arr.push(item.name);
//             arr.push(item.type);
//             arr.push(item.payment);
//             arr.push(item.price);
//             arr.push(item.discount_price);
//             arr.push(item.merchant_id);
//             arr.push(item.merchant_name);
//             arr.push(item.state);
//             arr.push((item.quantity_limit - item.quantity_sell_success));
//             arr.push(item.quantity_available);
//             arr.push(item.quantity_pending);
//             list.push(arr);
//         }
//         let buffer = xlsx.build([{name: "mySheetName", data: list}]);
//         let fileName = 'ListProduct.xlsx';

//         control.setHeader('Content-Disposition', `attachment; filename=${fileName}`);
//         control.setHeader('Content-Length', buffer.byteLength);
//         return buffer;
//     }
// });

// Route.post({
//     url: '/export/merchant',
//     summary: 'Xuất danh sách Merchant',
//     requireAuth: true,
//     permission: ['read_list_merchant'],
//     parameter: {
//         search: Types.string({description: 'từ khóa tìm kiếm theo tên hoặc ID merchant'}),
//         state: Types.string({description: 'trạng thái', enum: ['active', 'pending', 'delete', 'locked']}),
//         allowed_generate: Types.boolean({description: 'phát hành code từ lixi?'}),
//     },
//     response: Types.file(),
//     handle: async (control, route) => {
//         let es = Elastic.init({
//             index: 'res.company',
//             size: 10000,
//             sort: [
//                 {name_custom: {order: 'asc'}},
//             ],
//             filter_must_not: [],
//             filter_must: [],
//         });
//         if (route.args.search) {
//             if (Number.isInteger(Number(route.args.search)))
//                 es.filter_should.push({term: {id: route.args.search}});
//             es.filter_should.push({match: {name: {query: route.args.search.toLowerCase(), operator: 'and'}}});
//         }
//         if (route.args.state)
//             es.filter_must.push({term: {status: route.args.state}});

//         if ( route.args.allowed_generate !== null && route.args.allowed_generate === false) {
//             let listId = [];
//             let es_product = await Elastic.init({
//                 index: 'oc.shop',
//                 size: 10000,
//                 filter_must: [
//                     {term: {code_server: 'lixi'}},
//                     {term: {allow_generate: true}}
//                 ],
//             }).search();
//             for ( let i of es_product) {
//                 listId.push(i.merchant)
//             }

//             es.filter_must_not.push({terms: {id: listId}});
//         }
//         else if ( route.args.allowed_generate !== null && route.args.allowed_generate === true) {
//             let listId = [];
//             let es_product = await Elastic.init({
//                 index: 'oc.shop',
//                 size: 10000,
//                 filter_must: [
//                     {term: {code_server: 'lixi'}},
//                     {term: {allow_generate: true}}
//                 ],
//             }).search();
//             for ( let i of es_product) {
//                 listId.push(i.merchant)
//             }

//             es.filter_must.push({terms: {id: listId}});
//         }

//         let data = await es.search();

//         let result = await Promise.all(data.map(async x => {
//             let allowed_generate = 'Code do merchant phát hành';
//             let product = await Elastic.init({
//                 index: 'oc.shop',
//                 size: 10000,
//                 filter_must: [{term: {merchant: x.id}},],
//             }).search();

//             for (let item of product) {
//                 if(item.code_server && item.code_server === 'lixi' && item.allow_generate === true) {
//                     allowed_generate = 'Code do lixi phát hành';
//                     break;
//                 }
//             }

//             return {
//                 id: x.id,
//                 name: x.name,
//                 state: x.status,
//                 allowed_generate: allowed_generate,
//             }
//         }));
//         let list = [['ID', 'Merchant', 'Trạng thái','Phát hành code']];
//         for (let item of result) {
//             let arr = [];
//             arr.push(item.id);
//             arr.push(item.name);
//             arr.push(item.state);
//             arr.push(item.allowed_generate);
//             list.push(arr);
//         }
//         let buffer = xlsx.build([{name: "mySheetName", data: list}]);
//         let fileName = 'ListMerchant.xlsx';

//         control.setHeader('Content-Disposition', `attachment; filename=${fileName}`);
//         control.setHeader('Content-Length', buffer.byteLength);
//         return buffer;
//     }
// });

// Route.post({
//     url: '/export/merchant/store',
//     summary: 'Xuất danh sách cửa hàng của merchant',
//     requireAuth: true,
//     permission: ['read_list_merchant_store'],
//     parameter: {
//         merchant: {
//             // required: true,
//             type: Types.integer({description: 'ID merchant'}),
//         },
//         city_id: {
//             type: Types.integer({description: 'ID city'}),
//         },
//         search: Types.string({description: 'từ khóa tìm kiếm theo tên hoặc ID merchant store'}),
//         state: Types.string({description: 'trạng thái', enum: ['active', 'locked']}),
//     },
//     response: Types.file(),
//     handle: async (control, route) => {
//         let es = Elastic.init({
//             index: 'stock.warehouse',
//             size: 10000,
//             sort: [
//                 {priority: {order: 'desc'}},
//             ],
//             filter_must_not: [
//                 {term: {email: ''}}
//             ],
//             filter_must: [],
//             filter_should: [],
//         });
//         if (route.args.search) {
//             if (Number.isInteger(Number(route.args.search)))
//                 es.filter_should.push({term: {id: route.args.search}});
//             es.filter_should.push({match: {name: {query: route.args.search.toLowerCase(), operator: 'and'}}});
//         }
//         if (route.args.merchant)
//             es.filter_must.push({term: {merchant_id: route.args.merchant}});
//         if (route.args.state)
//             es.filter_must.push({term: {status: route.args.state}});
//         if (route.args.city_id)
//             es.filter_must.push({term: {city_id: route.args.city_id}});


//         let data = await es.search();
//         let result = await Promise.all(data.map(async x => {
//             let merchant = await Elastic.init({index: 'res.company', doctype: 'merchant'}).get(x.merchant_id);
//             return {
//                 id: x.id,
//                 name: x.name,
//                 state: x.status,
//                 merchant_id: merchant? merchant.id : null,
//                 merchant_name: merchant? merchant.name : null,
//             };
//         }));
//         let list = [['ID', 'Cửa hàng', 'Trạng thái', 'ID merchant', 'Merchant']];
//         for (let item of result) {
//             let arr = [];
//             arr.push(item.id);
//             arr.push(item.name);
//             arr.push(item.state);
//             arr.push(item.merchant_id);
//             arr.push(item.merchant_name);
//             list.push(arr);
//         }
//         let buffer = xlsx.build([{name: "mySheetName", data: list}]);
//         let fileName = 'ListStore.xlsx';

//         control.setHeader('Content-Disposition', `attachment; filename=${fileName}`);
//         control.setHeader('Content-Length', buffer.byteLength);
//         return buffer;
//     }
// });


// Route.post({
//     url: '/export/customer/list',
//     summary: 'Xuất danh sách tài khoản merchant',
//     requireAuth: true,
//     permission: ['read-list-merchant-customer'],
//     parameter: {
//         search: Types.string({description: 'từ khóa tìm kiếm theo tên hoặc ID tài khoản merchant'}),
//         level: Types.string({description: 'chức vụ'}),
//         merchant: Types.string({description: 'id merchant'}),
//     },
//     response: Types.file(),
//     handle: async (control, route) => {
//         let es = Elastic.init({
//             index: 'res.user',
//             size: 10000,
//             sort: [
//                 {fullname: {order: 'asc'}},
//             ],
//             filter_must: [],
//             filter_should: [],
//         });

//         if (route.args.search) {
//             if (Number.isInteger(Number(route.args.search)))
//                 es.filter_should.push({term: {id: route.args.search}});
//             es.filter_should.push({wildcard: {fullname: `*${route.args.search}*`}});
//             es.filter_should.push({wildcard: {username: `*${route.args.search}*`}});
//         }

//         if (route.args.level)
//             es.filter_must.push({term: {level: route.args.level}});
//         else
//             es.filter_must.push({terms: {level: ['cashier', 'manager']}});

//         if (route.args.merchant)
//             es.filter_must.push({terms: {company_ids: route.args.merchant.split(',')}});

//         let data = await es.search();
//         let result = await Promise.all(data.map(async (x) => {
//             let merchant = await Elastic.init({index: 'res.company', doctype: 'merchant'}).get(x.company_ids[0]);
//             return {
//                 id: x.id,
//                 name: x.fullname,
//                 login: x.username,
//                 level: x.level || null,
//                 merchant_id: merchant.id,
//                 merchant_name: merchant.name,
//             };
//         }));
//         let list = [['ID', 'Tên', 'Username', 'Chức vụ', 'ID merchant', 'Merchant']];
//         for (let item of result) {
//             let arr = [];
//             arr.push(item.id);
//             arr.push(item.name);
//             arr.push(item.login);
//             arr.push(item.level);
//             arr.push(item.merchant_id);
//             arr.push(item.merchant_name);
//             list.push(arr);
//         }
//         let buffer = xlsx.build([{name: "mySheetName", data: list}]);
//         let fileName = 'ListUsers.xlsx';

//         control.setHeader('Content-Disposition', `attachment; filename=${fileName}`);
//         control.setHeader('Content-Length', buffer.byteLength);
//         return buffer;
//     }
// });

// Route.post({
//     url: '/export/voucher/available',
//     summary: 'Xuất file excel evoucher available',
//     requireAuth: true,
//     permission: ['read_list_voucher'],
//     parameter: {
//         product: Types.string({description: 'ID product'}),
//     },
//     response: Types.file(),
//     handle: async (control, route) => {
//         let sql = `select *, Cast(count(*) Over() as Integer) As total_count from oc_evoucher_code 
//         where product = :product and state  in ('available', 'waiting')
//         Order By create_date desc Limit :pageSize Offset :pageOffset`;
        
//         let odooDb = OdooDb.init();
//         let data = await odooDb.query(sql, {
//             pageOffset: 0,
//             pageSize: 10000,
//             product: route.args.product,
//         });

//         let result = data.map(x => {
//             return {
//                 id: x.id,
//                 create_date: moment(x.create_date).format('YYYY-MM-DD HH:mm:ss'),
//                 pin: x.pin,
//                 state: x.state,
//                 expired_time: moment(x.expired_time).format('YYYY-MM-DD HH:mm:ss'),
//                 serial: x.serial,
//             }
//         });
//         let list = [['Thời gian tạo code ', 'Hết hạn', 'ID', 'Mã pin', 'Serial', 'Trạng thái']];
//         for (let item of result) {
//             let arr = [];
//             arr.push(item.create_date);
//             arr.push(item.expired_time);
//             arr.push(item.id);
//             arr.push(item.pin);
//             arr.push(item.serial);
//             arr.push(item.state);
//             list.push(arr);
//         }
//         let buffer = xlsx.build([{name: "mySheetName", data: list}]);
//         let fileName = 'ListVoucherAvailable.xlsx';

//         control.setHeader('Content-Disposition', `attachment; filename=${fileName}`);
//         control.setHeader('Content-Length', buffer.byteLength);
//         return buffer;
//     }
// });

// Route.post({
//     url: '/export/delivery/order',
//     summary: 'xuất file excel danh sách các giao dịch delivery',
//     // requireAuth: true,
//     // permission: ['read_list_shop_order'],
//     parameter: {
//         start_date: {required: true, type: Types.datetime({description: 'thời gian bắt đầu'})},
//         end_date: {required: true, type: Types.datetime({description: 'thời gian kết thúc'})},
//         uuid: Types.string({description: 'mã hóa đơn'}),
//         ref_code: Types.string({description: 'mã hóa đơn'}),
//         state: Types.string({description: 'trạng thái', enum: ['draft', 'payment', 'submitted', 'processing', 'confirmed', 'assigned', 'rejected', 'picked','expired', 'completed', 'canceled']}),
//         store_id: Types.integer({description: 'cửa hàng'}),
//         merchant_id: Types.integer({description: 'merchant'}),
//         user_id: Types.integer({description: 'khách hàng'}),
//         receive_phone: Types.string({description: 'số điện thoại'}),
//         states: Types.string({description: 'Những trạng thái'}),
//         gateway_code: Types.string({description: 'Cổng thanh toán'}),
//         product_id: Types.integer({description: 'ID sản phẩm'}),
//         first_time: Types.string({description: 'ID sản phẩm'}),
//         is_demo: Types.boolean({description: 'Demo?'}),
//         close_reason_type: Types.string({description: 'hình thức hủy đơn hàng', enum: ['user_cancel', 'driver_cancel', 'merchant_cancel']}),
//     },
//     response: Types.file(),
//     handle: async (control, route) => {

//         let list_uuid = [];
//         if (route.args.gateway_code) {
//             let payment_data = await DeliElastic.init({
//                 index: 'deli_payment_order',
//                 size: 10000,
//                 filter_must: [
//                     {term: {payment_method_code: route.args.gateway_code}},
//                     {range: {create_date: {gte: route.args.start_date.unix()*1000}}},
//                     {range: {create_date: {lte: route.args.end_date.unix()*1000}}},
//                 ],
//             }).search();

//             for(let item of payment_data) {
//                 list_uuid.push(item.user_order_uuid);
//             }
//         }
        
//         let store_demo = [];
//         if (route.args.is_demo !== null) {
//             let es_s = await DeliElastic.init({
//                 index: 'deli_store',
//                 size: 10,
//                 filter_must: [
//                     {term: {is_demo: true}},
//                 ],
//             }).search();

//             for(let item of es_s) {
//                 store_demo.push(item.id);
//             }
//         }
        
//         let es = DeliElastic.init({
//             index: 'deli_user_order',
//             start: 0,
//             size: 10000,
//             sort: [
//                 {submit_date: {order: 'desc'}},
//                 {receive_date: {order: 'desc'}},
//                 {create_date: {order: 'desc'}},
//             ],
//             filter_must_not: [],
//             filter_must: [
//                 {range: {create_date: {gte: route.args.start_date.unix()*1000}}},
//                 {range: {create_date: {lte: route.args.end_date.unix()*1000}}},
//             ],
//             filter_should: [],
//             // aggs: {
//             //     // list_store_id: {
//             //     //     terms: {field: 'store_id',size : 10000},
//             //     // },
//             //     list_user_id: {
//             //         terms: {field: 'user_id',size : 10000},
//             //     },
//             // }
//         });
       
//         if (route.args.uuid)
//             es.filter_must.push({term: {uuid: route.args.uuid}});
//         if (route.args.ref_code)
//             es.filter_must.push({term: {ref_code: route.args.ref_code}});
//         if (route.args.state)
//             es.filter_must.push({term: {state: route.args.state}});
//         if (list_uuid.length !== 0)
//             es.filter_must.push({terms: {uuid: list_uuid}});
//         if (route.args.states)
//             es.filter_must.push({terms: {state: ['submitted', 'confirmed']}});
//         if (route.args.merchant_id)
//             es.filter_must.push({term: {merchant_id: route.args.merchant_id}});
//         if (route.args.store_id)
//             es.filter_must.push({term: {store_id: route.args.store_id}});
//         else if (store_demo && route.args.is_demo)
//             es.filter_must.push({terms: {store_id: store_demo}});
//         else if (store_demo && !route.args.is_demo)
//             es.filter_must_not.push({terms: {store_id: store_demo}});

//         if (route.args.user_id)
//             es.filter_must.push({term: {user_id: route.args.user_id}});
//         if (route.args.receive_phone)
//             es.filter_must.push({term: {receive_phone: route.args.receive_phone}});
//         if (route.args.close_reason_type)
//             es.filter_must.push({term: {close_reason_type: route.args.close_reason_type}});

//         let data = await es.search();
//         // let data_count = await es.aggregate();

//         // let list_store_result = await DeliElastic.init({
//         //     index: 'deli_store',
//         //     size: 10000,
//         //     filter_must: [{terms: {id: data_count.list_store_id.buckets.map(x => x.key)}}],
//         //     filter_should: [],
//         // }).search();

//         // let list_user_result = await Elastic.init({
//         //     index: 'res.user',
//         //     size: 10000,
//         //     filter_must: [{terms: {id: data_count.list_user_id.buckets.map(x => x.key)}}],
//         //     filter_should: [],
//         // }).search();

        
        

//         let result = await Util.mapAwait(data, async(x) => {
//             // let store = null;
//             // let user = null;
//             // for(let item of list_store_result) {
//             //     if(item.id === x.store_id)
//             //         store = item;
//             // }
//             // let user = await Elastic.init({index: 'res.user', doctype: 'user'}).get(x.user_id);
//             let store = await DeliElastic.init({index: 'deli_store', doctype: 'deli_store'}).get(x.store_id);
//             let summary = 0;
//             let line = [];
//             let cs = null;

//             // for(let item of list_user_result) {
//             //     if(item.id === x.user_id) {
//             //         user = item;
//             //     }
//             // }

//             // let account = null;
//             // if(cs) {
//             //     for (let item of data_account.rows) {
//             //         if(item.id === cs.account_id) {
//             //             account = item;
//             //         }
//             //     }
//             // }

//             let product_money,service_money,ship_money,promotion_money;
//             let quotations = x.list_quotation.map( q => {
//                 if (q.code === 'product_money') {
//                     product_money = q.money;
//                 }
//                 else if (q.code === 'service_money') {
//                     service_money = q.money;
//                 }
//                 else if (q.code === 'promotion_money') {
//                     promotion_money = q.money;
//                 }
//                 else if (q.code === 'ship_money') {
//                     ship_money = q.money;
//                 }
//             });
            
//             return {
//                 date: x.submit_date ? moment(x.submit_date).format('DD-MM-YYYY HH:mm') : moment(x.create_date).format('DD-MM-YYYY HH:mm'),
//                 uuid: x.uuid,
//                 ref_code: x.ref_code,
//                 state: x.state,
//                 store_id: x.store_id,
//                 store_name: store? store.name : null,
//                 store_phone: store? store.phone : null,
//                 store_address: store? store.address : null,
//                 lixi_store_id: store? store.lixi_store_id : null,
//                 trusted_store: store? store.trusted_store : null,
//                 // user_id: x.user_id,
//                 // user_name: user? user.fullname : null,
//                 // user_phone: user? user.phone : null,
//                 receive_name: x.receive_name,
//                 receive_phone: x.receive_phone,
//                 receive_address: x.receive_address,
//                 receive_lat: x.receive_lat,
//                 receive_lng: x.receive_lng,
//                 receive_date: x.complete_date ? moment(x.complete_date).format('DD-MM-YYYY HH:mm') : moment(x.receive_date).format('DD-MM-YYYY HH:mm'),
//                 money: x.money,
//                 cashback: x.cashback,
//                 shipping_distance: x.shipping_distance,
//                 shipping_duration: x.shipping_duration ? Math.ceil(x.shipping_duration/60) : 0,
//                 note: x.note,
//                 close_reason_type: x.close_reason_type,
//                 close_reason_message: x.close_reason_message,
//                 quotation_product_money: product_money,
//                 quotation_service_money: service_money,
//                 quotation_ship_money: ship_money,
//                 quotation_promotion_money: promotion_money,
//                 // cs_id: account? account.id : null,
//                 // cs_name: account? account.name : null,
//                 // cs_login: account? account.login : null,
//                 is_as_soon_as_possible: x.is_as_soon_as_possible,
//             }
//         },100);


//         let list = [['Mã đơn hàng', 'Thời gian đặt', 'Thời gian nhận', 'Trạng thái', 'Lý do hủy', 'Phí vận chuyển', 'Phí dịch vụ', 'Phí đặt món', 'Giảm giá', 'Cashback', 'Tổng tiền', 'Người đặt hàng', 'SĐT người đặt', 'Nơi nhận', 'Cửa hàng', 'Địa chỉ cửa hàng']];
//         for (let item of result) {
//             let arr = [];
//                 arr.push(item.ref_code),
//                 arr.push(item.date),
//                 arr.push(item.receive_date),
//                 arr.push(item.state),
//                 arr.push(item.state === 'canceled' ? item.close_reason_message ? item.close_reason_message : 'Không lý do' : null),
//                 arr.push(item.quotation_ship_money),
//                 arr.push(item.quotation_service_money),
//                 arr.push(item.quotation_product_money),
//                 arr.push(item.quotation_promotion_money),
//                 arr.push(item.cashback),
//                 arr.push(item.money),
//                 arr.push(item.receive_name),
//                 arr.push(item.receive_phone),
//                 arr.push(item.receive_address),
//                 arr.push(item.store_name),
//                 arr.push(item.store_address),
//                 // arr.push(item.cs_name),
//                 list.push(arr)
//         }

//         let buffer = xlsx.build([{name: "Lịch sử giao hàng", data: list}]);
//         let fileName = 'ListDeliveryOrder.xlsx';

//         control.setHeader('Content-Disposition', `attachment; filename=${fileName}`);
//         control.setHeader('Content-Length', buffer.byteLength);
//         return buffer;
//     }
// });

// Route.post({
//     url: '/export/delivery/store/list',
//     summary: 'xuất file excel danh sách các cửa hàng delivery',
//     // requireAuth: true,
//     // permission: ['read_list_shop_order'],
//     parameter: {
//         merchant: {
//             type: Types.integer({description: 'ID merchant'}),
//         },
//         search: Types.string({description: 'từ khóa tìm kiếm theo tên hoặc ID merchant store'}),
//         state: Types.string({description: 'trạng thái', enum: ['active', 'inactive', 'paused']}),
//         is_demo: Types.boolean({description: 'cửa hàng thử nghiệm'}),
//         total: Types.integer({description: 'tổng số cửa hàng'}),
//         city: Types.string({description: 'thanh phố'}),
//     },
//     response: Types.file(),
//     handle: async (control, route) => {
//         let sql = `select
//                     s.id as 'id',
//                     s.name as 'name',
//                     s.priority as 'priority',
//                     s.phone as 'phone',
//                     s.state as 'state',
//                     s.merchant_id as 'merchant_id',
//                     s.address as 'address',
//                     substring_index(substring_index(s.address, ',', length(s.address) - length(replace(s.address, ',', '')) - 0), ',', -1) as 'district',
//                     s.commission_percent as 'commission_percent',
//                     s.trusted_store as 'trusted_store',
//                     s.promotion_text as 'promotion_text',
//                     s.count_complete_order as 'now_vn_order_count',
//                     s.min_price as 'min_price',
//                     s.max_price as 'max_price',
//                     date_format(date_add(open_time.open_time, interval 7 hour), '%H:%i') as 'open_time',
//                     date_format(date_add(open_time.close_time, interval 7 hour), '%H:%i') as 'close_time'
//                 from store as s
//                     join store_open_time as open_time on open_time.store_id = s.id and open_time.weekday = 1
//                 where s.is_demo = :is_demo `;

//         if (route.args.search && Number.isInteger(Number(route.args.search)))
//             sql += `and s.id = :search `;
//         if (route.args.search && !Number.isInteger(Number(route.args.search)))
//             sql += `and s.name like '%${route.args.search}%' `;
//         if (route.args.merchant)
//             sql += 'and s.merchant_id = :merchant ';
//         if (route.args.city)
//             sql += `and s.address like '%${route.args.city}%' `;
//         if (route.args.state)
//             sql += 'and s.state = :state ';
            
//         sql += 'Order By s.count_complete_order desc';

//         let deliDb = DeliDb.init();

//         let data = await deliDb.query(sql, {
//             search: route.args.search,
//             merchant: route.args.merchant,
//             is_demo: route.args.is_demo ? 1 : 0,
//             state: route.args.state,
//             city: route.args.city,
//         });


//         // let es = DeliElastic.init({
//         //     index: 'deli_store',
//         //     size: 10000,
//         //     sort: [
//         //         {count_complete_order: {order: 'desc'}},
//         //     ],
//         //     filter_must_not: [],
//         //     filter_must: [],
//         //     filter_should: [],
//         // });
//         // if (route.args.search) {
//         //     if (Number.isInteger(Number(route.args.search)))
//         //         es.filter_should.push({term: {id: route.args.search}});
//         //     es.filter_should.push({match: {name: {query: route.args.search.toLowerCase(), operator: 'and'}}});
//         // }
//         // if (route.args.merchant)
//         //     es.filter_must.push({term: {merchant_id: route.args.merchant}});
//         // if (route.args.state)
//         //     es.filter_must.push({term: {state: route.args.state}});
//         // if (route.args.is_demo !== null)
//         //     es.filter_must.push({term: {is_demo: route.args.is_demo}});

//         // let data = await es.count();
//         let sql_m = `Select * From res_company`;

//         let odooDb = OdooDb.init();
//         let data_m = await odooDb.query(sql_m);

//         let result = await Promise.all(data.map(async x => {
//             let merchant_name = null;
//             // let merchant = await Elastic.init({index: 'res.company', doctype: 'merchant'}).get(x.merchant_id);
//             for(let item of data_m) {
//                 if(item.id === x.merchant_id) {
//                     merchant_name = item.name;
//                 }
//             }
//             return {
//                 id: x.id,
//                 name: x.name,
//                 mode: 'delivery',
//                 state: x.state,
//                 is_demo: x.is_demo === true ? 'có' : 'không',
//                 is_hide: x.is_hide === true ? 'có' : 'không',
//                 promotion_text: x.promotion_text,
//                 trusted_store: x.trusted_store,
//                 commission_percent: x.commission_percent,
//                 priority: x.priority,
//                 address: x.address,
//                 phone: x.phone,
//                 district: x.district,
//                 now_vn_order_count: x.now_vn_order_count,
//                 min_price: x.min_price,
//                 max_price: x.max_price,
//                 open_time: x.open_time,
//                 close_time: x.close_time,
//                 merchant_name: merchant_name,
//             };
//         }));

//         let list = [['ID', 'Tên cửa hàng', 'Trạng thái', 'Số điện thoại', 'Địa chỉ', 'Quận', 'Nội dung giảm giá', 'Cửa hàng liên kết', 'Chiết khấu', 'Độ ưu tiên', 'Số giao dịch Now.vn', 'Merchant', 'Cửa hàng kiểm thử', 'Cửa hàng ẩn','Giá tối thiểu','Giá tối đa','Giờ mở cửa','Giờ đóng cửa']];
//         for (let item of result) {
//             let arr = [];
//                 arr.push(item.id),
//                 arr.push(item.name),
//                 arr.push(item.state),
//                 arr.push(item.address),
//                 arr.push(item.district),
//                 arr.push(item.phone),
//                 arr.push(item.promotion_text),
//                 arr.push(item.trusted_store),
//                 arr.push(item.commission_percent),
//                 arr.push(item.priority),
//                 arr.push(item.now_vn_order_count),
//                 arr.push(item.merchant_name),
//                 arr.push(item.is_demo),
//                 arr.push(item.is_hide),
//                 arr.push(item.min_price),
//                 arr.push(item.max_price),
//                 arr.push(item.open_time),
//                 arr.push(item.close_time),
//                 list.push(arr)
//         }

//         let buffer = xlsx.build([{name: "mySheetName", data: list}]);
//         let fileName = 'ListDeliveryStore.xlsx';

//         control.setHeader('Content-Disposition', `attachment; filename=${fileName}`);
//         control.setHeader('Content-Length', buffer.byteLength);
//         return buffer;
//     }
// });

// Route.post({
//     url: '/export/delivery/feedback/list',
//     summary: 'Xuất file excel delivery user order feddback',
//     // requireAuth: true,
//     // permission: ['read_list_delivery_feedback'],
//     parameter: {
//         start_date: {required: true, type: Types.datetime({description: 'thời gian bắt đầu'})},
//         end_date: {required: true, type: Types.datetime({description: 'thời gian kết thúc'})},
//         merchant: Types.integer({description: 'ID merchant'}),
//         rating: Types.string({description: 'rating point'}),
//         receive_phone: Types.string({description: 'receive_phone'}),
//         store_id: Types.integer({description: 'ID tìm kiếm theo cửa hàng'}),
//         type: Types.string({description: 'danh sách phân loại  (food, other, shipper, ship_price, ship_speed)'}),
//     },
//     response: Types.file(),
//     handle: async (control, route) => {
//         let list_uo = null;
//         if (route.args.receive_phone) {
//             let uo = DeliElastic.init({
//                 index: 'deli_user_order',
//                 filter_should: [
//                     // {terms: {merchant: args.merchant.split(',')}},
//                     {term: {receive_phone: route.args.receive_phone}},
//                     {term: {receive_phone: Util.phoneChangeTo11(route.args.receive_phone)}},
//                 ],
//                 aggs: {
//                     data: {terms: {field: 'uuid'}},
//                 }
//             });
//             uo = await uo.aggregate();
//             list_uo = uo.data.buckets.map(x => x.key);
//         }

//         let es = DeliElastic.init({
//             index: 'deli_user_order_feedback',
//             size: 10000,
//             sort: [
//                 {create_date: {order: 'desc'}},
//             ],
//             filter_must_not: [],
//             filter_must: [
//                 {range: {create_date: {gte: route.args.start_date.unix()*1000}}},
//                 {range: {create_date: {lte: route.args.end_date.unix()*1000}}},
//             ],
//             filter_should: [],
//             aggs: {
//                 list_store_id: {
//                     terms: {field: 'store_id',size : 10000},
//                 },
//                 list_user_order_uuid: {
//                     terms: {field: 'user_order_uuid',size : 10000},
//                 },
//                 list_merchant_id: {
//                     terms: {field: 'merchant_id',size : 10000},
//                 },
//                 summary: {
//                     terms: {field: 'rating',size : 10000},
//                 },
//                 // merchantCount: {cardinality: {field: 'merchant_id'}},
//             }
//         });
        
//         if (list_uo)
//             es.filter_must.push({terms: {user_order_uuid: list_uo}});
//         if (route.args.merchant)
//             es.filter_must.push({term: {merchant_id: route.args.merchant}});
//         if (route.args.rating)
//             es.filter_must.push({terms: {rating: route.args.rating.split(',')}});
//         if (route.args.store_id)
//             es.filter_must.push({term: {store_id: route.args.store_id}});
//         if (route.args.type)
//             es.filter_must.push({nested: {path: 'list_detail', query: {terms: {'list_detail.type_code' : route.args.type.split(',')}}}});

//         let data = await es.search();
//         let data_count = await es.aggregate();

//         let count1 = count2 = count3 = count4 = count5 = 0;

//         data_count.summary.buckets.map(x => {
//             if(x.key === 1)
//                 count1 = x.doc_count;
//             else if(x.key === 2)
//                 count2 = x.doc_count;
//             else if(x.key === 3)
//                 count3 = x.doc_count;
//             else if(x.key === 4)
//                 count4 = x.doc_count;
//             else if(x.key === 5)
//                 count5 = x.doc_count;

//         })

//         let list_store_result = await DeliElastic.init({
//             index: 'deli_store',
//             size: 10000,
//             filter_must: [{terms: {id: data_count.list_store_id.buckets.map(x => x.key)}}],
//             filter_should: [],
//         }).search();

//         let list_uo_result = await DeliElastic.init({
//             index: 'deli_user_order',
//             size: 10000,
//             filter_must: [{terms: {uuid: data_count.list_user_order_uuid.buckets.map(x => x.key)}}],
//             filter_should: [],
//         }).search();

//         let list_merchant_result = await Elastic.init({
//             index: 'res.company',
//             size: 10000,
//             filter_must: [{terms: {id: data_count.list_merchant_id.buckets.map(x => x.key)}}],
//             filter_should: [],
//         }).search();
        
//         let result = await Promise.all(data.map(async x => {
//             let merchant = null;
//             let store = null;
//             let user_order = null;
//             for(let item of list_store_result) {
//                 if(item.id === x.store_id)
//                     store = item;
//             }
//             for(let item of list_uo_result) {
//                 if(item.uuid === x.user_order_uuid)
//                     user_order = item;
//             }
//             for(let item of list_merchant_result) {
//                 if(item.id === x.merchant_id)
//                     merchant = item;
//             }

//             let string = '';
//             if(x.list_detail.length > 1) {
//                 for(let item of x.list_detail) {
//                     if(item.type_code === 'food')
//                         string = string + ' Chất lượng thức ăn,';
//                     else if(item.type_code === 'other')
//                         string = string + ' Khác,';
//                     else if(item.type_code === 'shipper')
//                         string = string + ' Đánh giá tài xế,';
//                     else if(item.type_code === 'ship_price')
//                         string = string + ' Phí vận chuyển,';
//                     else if(item.type_code === 'ship_speed')
//                         string = string + ' Tốc độ giao hàng,';
//                 }
//             }
//             else {
//                 for(let item of x.list_detail) {
//                     if(item.type_code === 'food')
//                         string = 'Chất lượng thức ăn';
//                     else if(item.type_code === 'other')
//                         string = 'Khác';
//                     else if(item.type_code === 'shipper')
//                         string = 'Đánh giá tài xế';
//                     else if(item.type_code === 'ship_price')
//                         string = 'Phí vận chuyển';
//                     else if(item.type_code === 'ship_speed')
//                         string = 'Tốc độ giao hàng';
//                 }
//             }

            
//             return {
//                 id: x.id,
//                 user_order_uuid: x.user_order_uuid,
//                 rating: x.rating,
//                 message: x.message,
//                 type: string,
//                 date: moment(x.create_date).add(moment().utcOffset(), 'minutes').format('DD-MM-YYYY HH:mm'),
//                 list_detail: x.list_detail,
//                 store: store ? store : null,
//                 merchant: merchant ? merchant : null,
//                 user_order: user_order ? user_order : null,
//             };
//         }));




//         let list = [['Thời gian ', 'ID khách hàng', 'Tên khách hàng', 'SĐT khách hàng', 'Mã đơn hàng', 'Cửa hàng', 'Merchant', 'Thang điểm (tối da 5)', 'Nội dung', 'Phân loại']];

//         for (let item of result) {
//             let arr = [];
//             arr.push(item.date);
//             arr.push(item.user_order ? item.user_order.user_id : null);
//             arr.push(item.user_order ? item.user_order.receive_name : null);
//             arr.push(item.user_order ? item.user_order.receive_phone : null);
//             arr.push(item.user_order ? item.user_order.ref_code : null);
//             arr.push(item.store ? item.store.name : null);
//             arr.push(item.merchant ? item.merchant.name : null);
//             arr.push(item.rating);
//             arr.push(item.message);
//             arr.push(item.type);
//             list.push(arr);

//             // if(item.rating === 1) {
//             //     count1 += 1;
//             //     list1.push(arr);
//             // }
//             // else if(item.rating === 2) {
//             //     count2 += 1;
//             //     list2.push(arr);
//             // }
//             // else if(item.rating === 3) {
//             //     count3 += 1;
//             //     list3.push(arr);
//             // }
//             // else if(item.rating === 4) {
//             //     count4 += 1;
//             //     list4.push(arr);
//             // }
//             // else if(item.rating === 5) {
//             //     count5 += 1;
//             //     list5.push(arr);
//             // }
//         }
//         let buffer = xlsx.build([{name: "mySheetName", data: list}]);
//         let fileName = 'ListDeliveryFeedback.xlsx';

//         control.setHeader('Content-Disposition', `attachment; filename=${fileName}`);
//         control.setHeader('Content-Length', buffer.byteLength);
//         return buffer;
//     }
// });