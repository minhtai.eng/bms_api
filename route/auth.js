const Config = require('../config');
const Route = require('../core/route');
const Types = require('../core/types');
const Db = require('../model');

Route.post({
    url: '/auth/login',
    summary: 'đăng nhập hệ thống Lixi',
    parameter: {
        username: {
            required: true,
            type: Types.string({description: 'tên đăng nhập', allowNull: false})
        },
        password: {
            required: true,
            type: Types.string({description: 'mật khẩu đăng nhập', allowNull: false, trim: false})
        },
    },
    response: Types.object({
        id: Types.integer({description: 'ID user'}),
        name: Types.string({description: 'tên tài khoản'}),
        authorization: Types.string({description: 'session token'}),
    }),
    error: {
        login_invalid: 'thông tin đăng nhập không chính xác',
        account_invalid: 'tài khoản bị lỗi, khóa, không đủ quyền truy cập',
    },
    handle: async (control, route) => {
        //region [find account]

        let user = await Db.user.getByLogin(route.args.username, route.args.password);
        if (user === null)
            throw {code: 'login_invalid'};
        if (user.status === 'deactive')
            throw {code: 'account_invalid'};

        //endregion

        //region [create or update session]

        let session = await Db.session.getByAccountId(user.id);
        if (session === null) {
            session = await Db.session.create(user.id);
        } else {
            await Db.session.extendExpired(session);
        }

        //endregion

        return {
            id: user.id,
            name: user.name,
            authorization: session.key,
        };
    },
    logHandle: async (control, route, result, isError) => {
        let parameter = route.args;
        delete parameter.password;

        await control.writeLogAction({
            user_id: result.data ? result.data.id : null,
            parameter:parameter,
            response: result,
            isError: isError,
        })
    }
});

Route.delete({
    url: '/auth/logout',
    summary: 'đăng xuất hệ thống Lixi',
    requireAuth: true,
    handle: async (control, route) => {
        return Db.session.deleteByAccountId(route.session.accountId);
    }
});