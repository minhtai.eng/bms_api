const Route = require('../core/route');
const Types = require('../core/types');
const Db = require('../model');
const moment = require('moment');

Route.get({
    url: '/logalarm',
    summary: 'Lấy danh sách log alarm',
    // requireAuth: true,
    // permission: ['read_list_account'],
    paging: true,
    parameter: {
        search: Types.string({description: 'từ khóa tìm kiếm theo tên hoặc ID'}),
        start_date: { type: Types.datetime({description: 'thời gian bắt đầu'})},
        end_date: {type: Types.datetime({description: 'thời gian kết thúc'})},
        ischeck:  Types.integer(),
        id_string: Types.integer(),
        id_unit: Types.integer(),
        state:Types.string()
    },
    response: Types.list(Types.object({
        id: Types.integer({description: 'ID'}),
        datetime: Types.datetime({description: 'ngày tạo'}),
        id_unit:  Types.integer({description: 'id_unit'}),
        id_string:  Types.integer({description: 'id_string'}),
        statealarm: Types.string({description: 'statealarm'}),
        vol_unit: Types.number({description: 'vol_unit'}),
        resistor_unit: Types.number({description: 'resistor_unit'}),
        ischeck: Types.integer(),
        unit:Types.object({
            numbat:Types.string({description: 'numbat'}),
        }),
        string:Types.object({
            name_accu:Types.string({description: 'name_accu'}),
        }),
    })),
    handle: async (control, route) => {
        let filter = {
            search: route.args.search,
        };
        if (route.args.start_date && route.args.end_date){
            filter.datetime = {
                gte: route.args.start_date,
                lte: route.args.end_date,
            };
        }
        if (route.args.id_unit){
            filter.id_unit = route.args.id_unit;
        }
        if (route.args.id_string){
            filter.id_string = route.args.id_string;
        }
        if (route.args.ischeck){
            filter.ischeck = route.args.ischeck;
        }
        if(route.args.state){
            filter.statealarm = route.args.state;
        }

        let data = await Db.logalarm.list(route.args.page, route.args.page_size, filter);
        control.setPaging(data.count);
        return data.rows.map(x => ({
            id: x.id,
            datetime: moment(x.datetime).unix(),
            id_unit:   x.id_unit,
	        id_string:   x.id_string,
	        statealarm:  x.statealarm,
	        vol_unit:  x.vol_unit,
	        resistor_unit:  x.resistor_unit,
            ischeck:  x.ischeck,
            unit:{
                numbat:x.unit ? x.unit.numbat : 0
            },
            string:{
                name_accu:x.string ? x.string.name_accu : 0
            }
        }));
    }
});

Route.post({
    url: '/update/logalarm',
    summary: 'Cập nhật logalarm',
    requireAuth: true,
    parameter: {
        id: {
            required: true,
            type: Types.number()
        },
        ischeck: {
            required: true,
            type: Types.string()
        },
    },
     response: Types.raw(),
    error: {
        not_found: 'không tìm thấy dữ liệu',
    },
    handle: async (control, route) => {
        let kq = await Db.logalarm.update(route.args.id,route.args.ischeck);
        return kq;
    },
    logHandle: async (control, route, result, isError) => {
        let parameter = route.args;
        
        await control.writeLogAction({
            user_id: route.session.accountId ? route.session.accountId : null,
            parameter:parameter,
            response: result,
            isError: isError,
        })
    }
});

Route.post({
    url: '/alarm/checkall',
    summary: 'Cập nhật logalarm all',
    requireAuth: true,
    parameter: {
        id_string: {
            required: true,
            type: Types.number()
        }
    },
    response: Types.raw(),
    error: {
        not_found: 'không tìm thấy dữ liệu',
    },
    handle: async (control, route) => {
        let sqlString = "Update logalarm set ischeck = 0 where id_string = " + route.args.id_string;
        let kq = await Db.logalarm.excuteSql(sqlString);
        return kq;
    },
    logHandle: async (control, route, result, isError) => {
        let parameter = route.args;
        
        await control.writeLogAction({
            user_id: route.session.accountId ? route.session.accountId : null,
            parameter:parameter,
            response: result,
            isError: isError,
        })
    }
});

Route.post({
    url: '/update/logalarm_unit',
    summary: 'Cập nhật logalarm',
    requireAuth: true,
    parameter: {
        id_string: {
            required: true,
            type: Types.number()
        },
        id_unit: {
            required: true,
            type: Types.number()
        },
    },
     response: Types.raw(),
    error: {
        not_found: 'không tìm thấy dữ liệu',
    },
    handle: async (control, route) => {
        let sqlString = "Update logalarm set ischeck = 0 where id_string = " + 
        route.args.id_string + " and id_unit = " + route.args.id_unit ;
        let kq = await Db.logalarm.excuteSql(sqlString);
        return kq;
    },
    logHandle: async (control, route, result, isError) => {
        let parameter = route.args;
        
        await control.writeLogAction({
            user_id: route.session.accountId ? route.session.accountId : null,
            parameter:parameter,
            response: result,
            isError: isError,
        })
    }
});

Route.get({
    url: '/alarm_load_summery',
    summary: 'Lấy danh sách log alarm',
    response: Types.raw(),
    handle: async (control, route) => {
        let filter = {};
        let start_date = moment().add(-2, 'hours').utc();
        // filter.datetime = {
        //     gte: route.args.start_date,
        // };

        // if (route.args.id_string){
        //     filter.id_string = route.args.id_string;
        // }
        filter.id_string = 2;
        filter.ischeck = 1;

        let groupItem = {
            where: filter,
             attributes: [
                'statealarm',
                'id_unit',
              ],
              group: ['statealarm','id_unit']
           }
        let chartData = await Db.logalarm.listGroupAll(groupItem);

        return chartData;
    }
});