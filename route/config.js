const Route = require('../core/route');
const Types = require('../core/types');
const Db = require('../model');
const moment = require('moment');

Route.get({
    url: '/config/alarm',
    summary: 'Lấy thông tin cấu hình',
    response: Types.object({
        volmax: Types.number({description: 'tổng ticket'}),
        volmin: Types.number({description: 'tổng ticket'}),
        tempmax: Types.number({description: 'tổng ticket'}),
        tempmin: Types.number({description: 'tổng ticket'}),
        resmax: Types.number(),
        resmin: Types.number(),
    }),
    error: {
        not_found: 'không tìm thấy dữ liệu',
    },
    handle: async (control, route) => {
        let itemData = await Db.alarmconfig.getById(1);

        return {
            volmax: itemData.volmax,
            volmin: itemData.volmin,
            tempmax: itemData.tempmax,
            tempmin: itemData.tempmin,
            resmax:  itemData.resmax,
            resmin:  itemData.resmin,
        };
    }
});

Route.post({
    url: '/config/alarm/update',
    summary: 'cập nhật ngưỡng',
    requireAuth: true,
    parameter: {
        volmax: {
            required: true,
            type: Types.number({description: 'tên đăng nhập', allowNull: false})
        },
        volmin: {
            required: true,
            type: Types.number({description: 'tên đăng nhập', allowNull: false})
        },
        tempmax: {
            required: true,
            type: Types.number({description: 'tên đăng nhập', allowNull: false})
        },
        tempmin: {
            required: true,
            type: Types.number({description: 'tên đăng nhập', allowNull: false})
        },
        resmax: {
            required: true,
            type: Types.number({description: 'tên đăng nhập', allowNull: false})
        },
        resmin: {
            required: true,
            type: Types.number({description: 'tên đăng nhập', allowNull: false})
        },
    },
     response: Types.object({
        volmax: Types.number({description: 'tổng ticket'}),
        volmin: Types.number({description: 'tổng ticket'}),
        tempmax: Types.number({description: 'tổng ticket'}),
        tempmin: Types.number({description: 'tổng ticket'}),
        resmax: Types.number({description: 'tổng ticket'}),
        resmin: Types.number({description: 'tổng ticket'}),
    }),
    error: {
        not_found: 'không tìm thấy dữ liệu',
    },
    handle: async (control, route) => {
        //region [find account]

        let updateData = await Db.alarmconfig.update({
            id:1,
            volmax: route.args.volmax,
            volmin: route.args.volmin,
            tempmax: route.args.tempmax,
            tempmin: route.args.tempmin,
            resmax: route.args.resmax,
            resmin: route.args.resmin,
        });
        

        return {
            volmax: updateData.volmax,
            volmin: updateData.volmin,
            tempmax: updateData.tempmax,
            tempmin: updateData.tempmin,
            resmax: route.args.resmax,
            resmin: route.args.resmin,
        };
    },
    logHandle: async (control, route, result, isError) => {
        let parameter = route.args;
        
        await control.writeLogAction({
            user_id: route.session.accountId ? route.session.accountId : null,
            parameter:parameter,
            response: result,
            isError: isError,
        })
    }
});


Route.get({
    url: '/config/getbyname',
    summary: 'Lấy thông tin config',
    parameter: {
        name: {
            required: true,
            type: Types.string({allowNull: false})
        },
    },
    response: Types.object({
        id: Types.number(),
        name: Types.string(),
        value: Types.string(),
    }),
    error: {
        not_found: 'không tìm thấy dữ liệu',
    },
    handle: async (control, route) => {
        let itemData = await Db.config.getByName(route.args.name);

        return {
            id: itemData.id,
            name: itemData.name,
            value: itemData.value,
        };
    }
});

Route.post({
    url: '/config/updatevalue',
    summary: 'cập nhật config',
    requireAuth: true,
    parameter: {
        id: {
            required: true,
            type: Types.number({allowNull: false})
        },
        value: {
            required: true,
            type: Types.string({allowNull: false})
        },
    },
     response: Types.object({
        id: Types.number(),
        name: Types.string(),
        value: Types.string(),
    }),
    error: {
        not_found: 'không tìm thấy dữ liệu',
    },
    handle: async (control, route) => {
        //region [find account]

        let updateData = await Db.config.update(route.args.id,route.args.value);

        return {
            id: updateData.id,
            name: updateData.name,
            value: updateData.value
        };
    },
    logHandle: async (control, route, result, isError) => {
        let parameter = route.args;
        
        await control.writeLogAction({
            user_id: route.session.accountId ? route.session.accountId : null,
            parameter:parameter,
            response: result,
            isError: isError,
        })
    }
});

Route.post({
    url: '/remove/deleteolddata',
    summary: 'Xoa log data cu',
    requireAuth: true,
    parameter: {
        id_string: {
            required: true,
            type: Types.number()
        }
    },
    response: Types.raw(),
    error: {
        not_found: 'không tìm thấy dữ liệu',
    },
    handle: async (control, route) => {
        let sqlString = "delete from log_unit where datetime <= " + moment().subtract(3, 'months').format('YYYY-MM-DD HH:mm:ss');
        let kq = await Db.logalarm.excuteSql(sqlString);
        return kq;
    },
    logHandle: async (control, route, result, isError) => {
        let parameter = route.args;
        
        await control.writeLogAction({
            user_id: route.session.accountId ? route.session.accountId : null,
            parameter:parameter,
            response: result,
            isError: isError,
        })
    }
});