const BaseModel = require('../core/base-model');
const Util = require('../core/util');
const sequelize = require('sequelize');
const op = sequelize.Op;
const moment = require('moment');
module.exports = class extends BaseModel {
    static get _module() {
        return module;
    }

    static get _schema() {
        return {
            id: {type: sequelize.INTEGER, primaryKey: true, autoIncrement: true},
            datetime: sequelize.DATE,
            id_unit: sequelize.INTEGER,
            ampe: sequelize.FLOAT,
            vol: sequelize.FLOAT,
            time_discharge: sequelize.INTEGER,
            id_discharge: sequelize.INTEGER,
            momh: sequelize.FLOAT,            
            dodavg: sequelize.FLOAT,
            level: sequelize.FLOAT,
            temp: sequelize.FLOAT,
        };
    }

    static _associations(models) {
        this.model.belongsTo(models.battunit.model, {as: 'battunit',foreignKey: 'id_unit'});
    }

    static async list(page, pageSize, filter) {
        const Db = require('./index');

        //region [filter]

        let where = {};
        if (!filter)
            filter = {};

        if (filter.id)
            where.id = filter.id;
        if (filter.listid)
            where.id_unit =  {
              [op.or]: filter.listid
            };

        if (filter.id_unit)
            where.id_unit = filter.id_unit;

        if (filter.datetime)
            where.datetime = filter.datetime;

        if (filter.search) {
            let conditions = [];
            if (Number.isInteger(Number(filter.search)))
                conditions.push({id: parseInt(filter.search)});
            
            where[op.or] = conditions;
        }

        //endregion
        return await super.list(page, pageSize, {
            where: where,
            order: [['id', 'desc']],
            include: [
                        {
                            model: Db.battunit.model,
                            as: 'battunit'
                        }
                    ]
        });
    }

    static async getById(id) {
        return await this.model.findOne({
            where: {
                id: id,
            },
        });
    }

    static async create(id_unit, ampe,vol,time_discharge,id_discharge,momh,dodavg,level,temp) {
        return await this.model.create({
            datetime: moment(),
            id_unit: id_unit,
            ampe: ampe,
            vol: vol,
            time_discharge: time_discharge,
            id_discharge: id_discharge,
            momh: momh,            
            dodavg: dodavg,
            level: level,
            temp: temp,
        });
    }
    
};