const BaseModel = require('../core/base-model');
const Util = require('../core/util');
const sequelize = require('sequelize');
const op = sequelize.Op;
const moment = require('moment');
module.exports = class extends BaseModel {
    static get _module() {
        return module;
    }

    static get _schema() {
        return {
            id: {type: sequelize.BIGINT, primaryKey: true, autoIncrement: true},
            status: sequelize.STRING,
            name: sequelize.STRING,
            email: sequelize.STRING,
            password: sequelize.STRING,
            usertype: sequelize.STRING,
            usercode: sequelize.STRING,
        };
    }

    static async list(page, pageSize, filter) {
        const Db = require('./index');

        //region [filter]

        let where = {};
        if (!filter)
            filter = {};

        if (filter.id)
            where.id = filter.id;

        if (filter.name)
            where.name = {like: `%${filter.name}%`};

        if (filter.status)
            where.status = {like: `%${filter.status}%`};

        if (filter.search) {
            let conditions = [];
            if (Number.isInteger(Number(filter.search)))
                conditions.push({id: parseInt(filter.search)});
            conditions.push({name: {like: `%${filter.search}%`}});

            where[op.or] = conditions;
        }
        if(filter.cs_color_background){
            where.cs_color_background = filter.cs_color_background;
        }
        //endregion
        return await super.list(page, pageSize, {
            where: where,
            order: [['name', 'asc']],
        });
    }

    static async getById(id) {
        return await this.model.findOne({
            where: {
                id: id,
            },
        });
    }

    static async getByIds(ids) {
        return await this.model.findAll({
            where: {
                id: { [op.in]: ids },
            },
        });
    }

    static async list(page, pageSize, filter) {
        const Db = require('./index');

        //region [filter]

        let where = {};
        if (!filter)
            filter = {};

        if (filter.id)
            where.id = filter.id;
        if (filter.status)
            where.status = filter.status;
        if (filter.name)
            where.name = {like: `%${filter.name}%`};
            
        return await super.list(page, pageSize, {
            where: where,
            order: [['id', 'desc']],
        });
    }

    static async getByLogin(email, pass) {
        const Util = require('../core/util');

        return await this.model.findOne({
            where: {
                email: email,
                password: Util.sha3_512Hash(pass ? pass : ''),
            },
        });
    }

    static async updateProfile(id, data) {
        let account = await this.getById(id);
        account.name = data.name;
        await account.save();

        return account;
    }

    static async updatePassword(id, oldPass, newPass) {
        let account = await this.getById(id);

        if (Util.sha3_512Hash(oldPass) !== account.pass) {
            throw {
                code: 'old_pass_wrong',
                message: 'Old password is wrong',
            };
        }

        account.pass = Util.sha3_512Hash(newPass);
        await account.save();

        return account;
    }
};