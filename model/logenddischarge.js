const BaseModel = require('../core/base-model');
const Util = require('../core/util');
const sequelize = require('sequelize');
const op = sequelize.Op;
const moment = require('moment');
module.exports = class extends BaseModel {
    static get _module() {
        return module;
    }

    static get _schema() {
        return {
            id: {type: sequelize.INTEGER, primaryKey: true, autoIncrement: true},
            datetime: sequelize.DATE,
            id_unit: sequelize.INTEGER,
            ampe: sequelize.FLOAT,
            vol: sequelize.FLOAT,
            time_discharge: sequelize.INTEGER,
            dod: sequelize.FLOAT,
        };
    }

    static _associations(models) {
        this.model.belongsTo(models.battunit.model, {as: 'battunit',foreignKey: 'id_unit'});
    }

    static async list(page, pageSize, filter) {
        const Db = require('./index');

        //region [filter]

        let where = {};
        if (!filter)
            filter = {};

        if (filter.id)
            where.id = filter.id;

        if (filter.id_unit)
            where.id_unit = filter.id_unit;

        if (filter.datetime)
            where.datetime = filter.datetime;

        if (filter.search) {
            let conditions = [];
            if (Number.isInteger(Number(filter.search)))
                conditions.push({id: parseInt(filter.search)});
            
            where[op.or] = conditions;
        }

        //endregion
        return await super.list(page, pageSize, {
            where: where,
            order: [['id', 'asc']],
            include: [
                        {
                            model: Db.battunit.model,
                            as: 'battunit'
                        }
                    ]
        });
    }

    static async getById(id) {
        return await this.model.findOne({
            where: {
                id: id,
            },
        });
    }

    
};