const BaseModel = require('../core/base-model');
const Util = require('../core/util');
const sequelize = require('sequelize');
const op = sequelize.Op;
const moment = require('moment');
module.exports = class extends BaseModel {
    static get _module() {
        return module;
    }
    static get _schema() {
        return {
            id: {type: sequelize.INTEGER, primaryKey: true, autoIncrement: true},
            datetime: sequelize.DATE,
            id_unit: sequelize.INTEGER,
            id_string: sequelize.INTEGER,
            statealarm: sequelize.STRING,
            vol_unit: sequelize.FLOAT,
            resistor_unit: sequelize.FLOAT,
            ischeck: sequelize.INTEGER,
        };
    }

    static _associations(models) {
        // this.model.belongsTo(models.battstring.model, {foreignKey: 'id_battstring'});
        this.model.belongsTo(models.battunit.model, {as: 'unit',foreignKey: 'id_unit'});
        this.model.belongsTo(models.battstring.model, {as: 'string',foreignKey: 'id_string'});
    }

    static async list(page, pageSize, filter) {
        const Db = require('./index');

        //region [filter]

        let where = {};
        if (!filter)
            filter = {};

        if (filter.id)
            where.id = filter.id;

        if (filter.id_unit)
            where.id_unit = filter.id_unit;
         if (filter.id_string)
            where.id_string = filter.id_string;
        if (filter.ischeck)
            where.ischeck = filter.ischeck;
         if (filter.statealarm)
            where.statealarm = filter.statealarm;
        
        if (filter.search) {
            let conditions = [];
            if (Number.isInteger(Number(filter.search)))
                conditions.push({id: parseInt(filter.search)});
            
            where[op.or] = conditions;
        }

        //endregion
        return await super.list(page, pageSize, {
            where: where,
            order: [['id', 'asc']],
            include: [
                        {
                            model: Db.battunit.model,
                            as: 'unit',
                        },
                        {
                            model: Db.battstring.model,
                            as: 'string',
                        },
                    ]
        });
    }

    static async getById(id) {
        return await this.model.findOne({
            where: {
                id: id,
            },
        });
    }

    static async create(id_unit, id_string,statealarm,vol_unit,resistor_unit,ischeck) {
        return await this.model.create({
            datetime: moment(),
            id_unit: id_unit,
            id_string: id_string,
            statealarm: statealarm,
            vol_unit: vol_unit,
            resistor_unit: resistor_unit,
            ischeck: ischeck,
        });
    }

    static async update(id,ischeck) {
        let temp = await this.getById(id);
        temp.ischeck = ischeck;
        await temp.save();

        return temp;
    }
};