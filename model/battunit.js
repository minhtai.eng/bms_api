const BaseModel = require('../core/base-model');
const Util = require('../core/util');
const sequelize = require('sequelize');
const op = sequelize.Op;
const moment = require('moment');

module.exports = class extends BaseModel {
    static get _module() {
        return module;
    }

    static get _schema() {
        return {
            id: {type: sequelize.INTEGER, primaryKey: true, autoIncrement: true},
            datetime: sequelize.DATE,
            id_battstring: sequelize.INTEGER,
            numbat: sequelize.INTEGER,
            serial: sequelize.STRING,
            ip_adr:sequelize.STRING,
        };
    }

    static _associations(models) {
        // this.model.belongsTo(models.battstring.model, {foreignKey: 'id_battstring'});
        this.model.hasMany(models.log_unit.model, {as: 'current_value',foreignKey: 'id_unit'});
        this.model.hasMany(models.logalarm.model, {as: 'alarm',foreignKey: 'id_unit'});
    }

    static async list(page, pageSize, filter) {
        const Db = require('./index');

        //region [filter]

        let where = {};
        if (!filter)
            filter = {};

        if (filter.id)
            where.id = filter.id;

        if (filter.id_string)
            where.id_battstring = filter.id_string;

        // if (filter.search) {
        //     let conditions = [];
        //     if (Number.isInteger(Number(filter.search)))
        //         conditions.push({id: parseInt(filter.search)});
            
        //     where[op.or] = conditions;
        // }

        //endregion
        return await super.list(page, pageSize, {
            where: where,
            order: [['id', 'asc']],
        });
    }

    static async listwithlast(page, pageSize, filter) {
        const Db = require('./index');

        //region [filter]

        let where = {};
        if (!filter)
            filter = {};

        if (filter.id)
            where.id = filter.id;

        if (filter.id_string)
            where.id_battstring = filter.id_string;

        if (filter.search) {
            let conditions = [];
            if (Number.isInteger(Number(filter.search)))
                conditions.push({numbat: parseInt(filter.search)});
            
            where[op.or] = conditions;
        }

        //endregion
        return await super.list(page, pageSize, {
            where: where,
            order: [['id', 'asc']],
            include: [
                        {
                            model: Db.log_unit.model,
                            as: 'current_value',
                            order: [["id", "DESC"]],
                            limit: 1,
                        },
                        {
                            model: Db.logalarm.model,
                            as: 'alarm',
                            order: [["id", "DESC"]],
                            limit: 1,
                        }
                    ]
        });
    }

    static async listidunit(page, pageSize, id_string) {
        const Db = require('./index');

        //region [filter]

        let where = {};

        where.id_battstring = id_string;

        //endregion
        return await super.list(page, pageSize, {
            where: where,
            order: [['id', 'asc']],
            attributes: ['id']
        });
    }

    static async getById(id) {
        return await this.model.findOne({
            where: {
                id: id,
            },
        });
    }

    static async create(id_battstring, numbat, serial,ip_adr) {
        return await this.model.create({
            datetime: moment(),
            id_battstring: id_battstring,
            numbat: numbat,
            serial: serial,
            ip_adr:ip_adr
        });
    }

    static async update(id,id_battstring, numbat, serial,ip_adr) {
        let temp = await this.getById(id);
        temp.id_battstring = id_battstring;
        temp.numbat = numbat;
        temp.serial = serial;
        temp.ip_adr = ip_adr;
        await temp.save();

        return temp;
    }
};