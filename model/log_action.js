const BaseModel = require('../core/base-model');
const Util = require('../core/util');
const sequelize = require('sequelize');
const moment = require('moment');

module.exports = class extends BaseModel {
    static get _module() {
        return module;
    }

    static get _schema() {
        return {
            id: {type: sequelize.BIGINT, primaryKey: true, autoIncrement: true},
            user_id: sequelize.BIGINT,
            date: sequelize.DATE,           
            method: sequelize.STRING,
            url: sequelize.STRING,
            parameter: sequelize.STRING,
            response: sequelize.STRING,
            error: sequelize.BOOLEAN,
        };
    }

    static _associations(models) {
        this.model.belongsTo(models.user.model, {foreignKey: 'user_id'})
    }

    static async create(user_id, method, url, parameter, response, isError) {
        const Db = require('./index');

        return await this.model.create({
            user_id: user_id,
            date: moment(),
            method: method,
            url: url,
            parameter: JSON.stringify(parameter),
            response: JSON.stringify(response),
            error: isError,
        });
    }

    static async list(page, pageSize, filter) {
        const Db = require('./index');

        //region [filter]

        let where = {};
        if (!filter)
            filter = {};

        if (filter.user_id)
            where.user_id = filter.user_id;

        if (filter.id)
            where.id = filter.id;

        if (filter.startDate && filter.endDate)
            where.date = {
                gte: filter.startDate,
                lte: filter.endDate,
            };
        else if (filter.startDate)
            where.date = {gte: filter.startDate};
        else if (filter.endDate)
            where.date = {lte: filter.endDate};

        if (filter.method)
            where.method = {like: `%${filter.method}%`};

        if (filter.url)
            where.url = {like: `%${filter.url}%`};

        if (filter.error)
            where.error = filter.error;

        if (filter.parameter)
            where.parameter = {like: `%${filter.parameter}%`};

        if (filter.response)
            where.response = {like: `%${filter.response}%`};

        //endregion

        return await super.list(page, pageSize, {
            where: where,
            include: [{model: Db.user.model}],
            order: [['id', 'desc']],
        });
    }

    static async getById(id) {
        const Db = require('./index');

        return await this.model.findOne({
            include: [{model: Db.user.model}],
            where: {
                id: id,
            },
        });
    }

    // static async deleteOldItems() {
    //     const Db = require('./index');

    //     let log_action_timeout = await Db.configuration.getValueByKey('log_action_timeout');
    //     log_action_timeout = parseInt(log_action_timeout);

    //     let expiredDate = moment().subtract(log_action_timeout, 'days').toDate();
    //     return await this.model.destroy({
    //         where: {date: {lt: expiredDate}}
    //     });
    // }
};