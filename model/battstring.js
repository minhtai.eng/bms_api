const BaseModel = require('../core/base-model');
const Util = require('../core/util');
const sequelize = require('sequelize');
const op = sequelize.Op;
const moment = require('moment');

module.exports = class extends BaseModel {
    static get _module() {
        return module;
    }

    static get _schema() {
        return {
            id: {type: sequelize.INTEGER, primaryKey: true, autoIncrement: true},
            datetime: sequelize.DATE,
            name_accu: sequelize.STRING,
            location: sequelize.STRING,
            ip_add: sequelize.STRING,
            id_discharge: sequelize.INTEGER,
            ccquality:sequelize.INTEGER,
        };
    }

    static _associations(models) {
        this.model.hasMany(models.battunit.model, {as: 'listunit',foreignKey: 'id_battstring'});
        this.model.hasMany(models.log_string.model, {as: 'current_value',foreignKey: 'id_string'});
        //Project.hasMany(User, {as: 'Workers'})
    }

    static async list(page, pageSize, filter) {
        const Db = require('./index');

        //region [filter]

        let where = {};
        if (!filter)
            filter = {};

        if (filter.id)
            where.id = filter.id;

        if (filter.name)
            where.name_accu = {like: `%${filter.name}%`};

        if (filter.search) {
            let conditions = [];
            if (Number.isInteger(Number(filter.search)))
                conditions.push({id: parseInt(filter.search)});
            
            where[op.or] = conditions;
        }

        //endregion
        return await super.list(page, pageSize, {
            where: where,
            order: [['id', 'asc']],
        });
    }

    static async listwithunit(page, pageSize, filter) {
        const Db = require('./index');

        //region [filter]

        let where = {};
        if (!filter)
            filter = {};

        if (filter.id)
            where.id = filter.id;

        if (filter.name)
            where.name_accu = {like: `%${filter.name}%`};

        if (filter.search) {
            let conditions = [];
            if (Number.isInteger(Number(filter.search)))
                conditions.push({id: parseInt(filter.search)});
            
            where[op.or] = conditions;
        }

        //endregion
        return await super.list(page, pageSize, {
            where: where,
            order: [['id', 'asc']],
            include: [
                        {
                            model: Db.battunit.model,
                            as: 'listunit',
                            order: [["numbat", "asc"]],
                            include: [
                                    {
                                        model: Db.log_unit.model,
                                        as: 'current_value',
                                        order: [["id", "DESC"]],
                                        limit: 1,                                        
                                    }
                            ]
                        },{
                            model: Db.log_string.model,
                            as: 'current_value',
                            // where: {
                            //     id_battstring: { $in: Db.battstring.model.id }
                            // },
                            order: [["id", "DESC"]],
                            limit: 1,
                        }
                    ]

        });
    }

    static async getById(id) {
        return await this.model.findOne({
            where: {
                id: id,
            },
        });
    }

    static async create(name_accu, location, ip_add,id_discharge,ccquality) {
        return await this.model.create({
            datetime: moment(),
            name_accu: name_accu,
            location: location,
            ip_add: ip_add,
            id_discharge: id_discharge,
            ccquality:ccquality
        });
    }

    static async update(id,name_accu, location, ip_add,ccquality) {
        let temp = await this.getById(id);
        temp.name_accu = name_accu;
        temp.location = location;
        temp.ip_add = ip_add;
        temp.ccquality = ccquality;
        await temp.save();

        return temp;
    }
};