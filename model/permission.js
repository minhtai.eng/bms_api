const BaseModel = require('../core/base-model');
const Util = require('../core/util');
const sequelize = require('sequelize');

module.exports = class extends BaseModel {
    static get _module() {
        return module;
    }

    static get _schema() {
        return {
            id: {type: sequelize.BIGINT, primaryKey: true},
            name: {type: sequelize.STRING},
            user_id: {type: sequelize.STRING},
        };
    }

    static async listByAccountId(accountId) {
        const Db = require('./index');
        let permissions = await this.model.findAll({
            where: {
                user_id : accountId,
            },
        });

        let data = [];
        for (let permission of permissions)
        {
            data.push(permission.name);
        }
        return data;
    }
};