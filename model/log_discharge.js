const BaseModel = require('../core/base-model');
const Util = require('../core/util');
const sequelize = require('sequelize');
const op = sequelize.Op;
const moment = require('moment');

module.exports = class extends BaseModel {
    static get _module() {
        return module;
    }

    static get _schema() {
        return {
            id: {type: sequelize.INTEGER, primaryKey: true, autoIncrement: true},
            datetime: sequelize.DATE,
            id_string: sequelize.INTEGER,
            state: sequelize.STRING
        };
    }

    static _associations(models) {
        this.model.belongsTo(models.battstring.model, {as: 'battstring',foreignKey: 'id_string'});
    }

    static async list(page, pageSize, filter) {
        const Db = require('./index');

        //region [filter]

        let where = {};
        if (!filter)
            filter = {};

        if (filter.id)
            where.id = filter.id;

        if (filter.datetime)
            where.datetime = filter.datetime;

        if (filter.search) {
            let conditions = [];
            if (Number.isInteger(Number(filter.search)))
                conditions.push({id: parseInt(filter.search)});
            
            where[op.or] = conditions;
        }

        //endregion
        return await super.list(page, pageSize, {
            where: where,
            order: [['id', 'asc']],
            include: [
                        {
                            model: Db.battstring.model,
                            as: 'battstring'
                        }
                    ]
        });
    }

    static async getById(id) {
        return await this.model.findOne({
            where: {
                id: id,
            },
        });
    }

    static async getByStringIdLast(id_string) {
        return await this.model.findOne({
            where: {
                id_string: id_string,
            },
            order: [ [ 'id', 'DESC' ]]
        });
    }

    static async create(id_string, state) {
        return await this.model.create({
            datetime: moment(),
            id_string: id_string,
            state: state
        });
    }

    static async update(id,state) {
        let temp = await this.getById(id);
        temp.state = state;
        await temp.save();

        return temp;
    }
};