const BaseModel = require('../core/base-model');
const Util = require('../core/util');
const sequelize = require('sequelize');
const moment = require('moment');
const op = sequelize.Op;

module.exports = class extends BaseModel {
    static get _module() {
        return module;
    }
    static get _schema() {
        return {
            id: {type: sequelize.INTEGER, primaryKey: true, autoIncrement: true},
            datetime: sequelize.DATE,
            volmax: sequelize.FLOAT,
            volmin: sequelize.FLOAT,
            tempmax: sequelize.FLOAT,
            tempmin: sequelize.FLOAT,
            resmax: sequelize.FLOAT,
            resmin: sequelize.FLOAT
        };
    }

    static _associations(models) {
        // this.model.belongsTo(models.battstring.model, {foreignKey: 'id_battstring'});
    }

    static async list(page, pageSize, filter) {
        const Db = require('./index');

        //region [filter]

        let where = {};
        if (!filter)
            filter = {};

        if (filter.id)
            where.id = filter.id;

        if (filter.id_unit)
            where.id_unit = filter.id_unit;


        
        if (filter.search) {
            let conditions = [];
            if (Number.isInteger(Number(filter.search)))
                conditions.push({id: parseInt(filter.search)});
            
            where[op.or] = conditions;
        }

        //endregion
        return await super.list(page, pageSize, {
            where: where,
            order: [['id', 'asc']],
        });
    }

    static async create(item){
    	return await this.model.create({
    		datetime: moment(),
            volmax: item.volmax,
            volmin: item.volmin,
            tempmax: item.tempmax,
            tempmin: item.tempmin,
            resmax: item.resmax,
            resmin: item.resmin
    	});
    }

    static async update(item) {
        let updateitem = await this.getById(item.id);

        updateitem.datetime = moment();
        updateitem.volmax = item.volmax;
        updateitem.volmin = item.volmin;
        updateitem.tempmax = item.tempmax;
        updateitem.tempmin = item.tempmin;
        updateitem.resmax = item.resmax;
        updateitem.resmin = item.resmin;
        await updateitem.save();

        return updateitem;
    }

    static async getById(id) {
        return await this.model.findOne({
            where: {
                id: id,
            },
        });
    }
    
};