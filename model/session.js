const Config = require('../config');
const BaseModel = require('../core/base-model');
const Util = require('../core/util');
const jwt = require('jsonwebtoken');
const sequelize = require('sequelize');
const moment = require('moment');

module.exports = class extends BaseModel {
    static get _module() {
        return module;
    }

    static get _schema() {
        return {
            id_user: {type: sequelize.BIGINT, primaryKey: true},
            key: sequelize.STRING,
            expired: sequelize.DATE,
        };
    }

    static async getByAccountId(accountId) {
        return await this.model.findOne({
            where: {
                id_user: accountId,
            },
        });
    }

    static async getByKey(key) {
        return await this.model.findOne({
            where: {
                key: key,
            },
        });
    }

    static async create(accountId) {
        const Db = require('./index');

        let user = await Db.user.getById(accountId);
        // let permissions = await Db.permission.listByAccountId(accountId);
        // let session_timeout = await Db.configuration.getValueByKey('session_timeout');

        let token = jwt.sign({
            id: user.id,
            email: user.email,
        }, Config.jwt_secret_key);

        return await this.model.create({
            id_user: accountId,
            key: token,
            expired: moment().add(60*24*15, 'minutes'),
        });
    }

    static async extendExpired(session) {
        const Db = require('./index');

        // let session_timeout = await Db.configuration.getValueByKey('session_timeout');

        return await session.update({
            expired: moment().add(60*24*15, 'minutes'),
        });
    }

    static async delete(session) {
        return await session.destroy();
    }

    static async deleteByAccountId(accountId) {
        let session = await this.getByAccountId(accountId);
        return await this.delete(session);
    }
};