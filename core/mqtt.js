var version = "20200526";
let Moment = require('moment');
const mqtt = require('mqtt');
let config = {
	// 'mqtt' : {
	// 	host : 'mqtt://115.84.181.250',
	// 	port: 1883
	// }
	options: {
	  hostname: 'localhost',
	  port: 1883,
	  protocol: 'mqtt',
	}
};
let subscribe = 'log_bms/#'
// let InsertLogGroup = Hub.getEndpoint('log.insert_group_log');
// let GetGroupById = Hub.getEndpoint('group.get_by_Id');
// let GetLastCharge = Hub.getEndpoint('log.get_last_charge');
// let InsertLogCharge = Hub.getEndpoint('log.insert_group_charge');
// let InsertLogUnit = Hub.getEndpoint('log.insert_unit');
// let GetAlarmConfig = Hub.getEndpoint('config.get_alarm');
// let InsertLogAlarm = Hub.getEndpoint('log.insert_alarm');
// let UpdateLogCharge = Hub.getEndpoint('log.update_log_charge');

const Db = require('../model');
module.exports = {
	start: () => Start()
}
function Start() {
    
    // let mqttClient = mqtt.connect(config.options);
    let mqttClient = mqtt.connect('mqtt://localhost:1883', { username: null, password: null });


    mqttClient.on('error', (err) => {
      console.log(err);
      mqttClient.end();
    });

    mqttClient.on('connect', () => {
      console.log(`mqtt client connected`);
      mqttClient.subscribe(subscribe, {qos: 0});
    });

    mqttClient.on('message', async function (topic, message) {
        let now = Moment().toDate();
        let objData = '';
        try {
            objData = JSON.parse(message.toString());

            // let alarmConfig = await GetAlarmConfig({ id: 1 });
            let alarmConfig = await Db.alarmconfig.getById(1);
            if(objData && (objData.return == 200 || objData.return == 201 )){
                let current_ampe = objData.I;
                console.log("current_ampe : " + current_ampe);
                // let group_unit = await GetGroupById({ id : 1 });
                let group_unit = await Db.battstring.getById(1);
                if(group_unit){
                    // thêm log tổ
                    // await InsertLogGroup({
                    //     status : objData.return,
                    //     group_id : group_unit.id,
                    //     group_name : group_unit.name,
                    //     ampe : objData.I,
                    //     vol : objData.U
                    // });
                    await Db.log_string.create(1,objData.I,objData.U,0,0);
        
                    // let lastCharge = await GetLastCharge();
                    let lastCharge = await Db.log_discharge.getByStringIdLast(1);

                    if(lastCharge && lastCharge.state === 'charge'){
                    	console.log("charge");
                    	let charge_id = lastCharge.id;
                        if(current_ampe > 0){ // đang xả => bắt đầu ghi trạng thái xả
                            // let discharge = await InsertLogCharge({ group_id : group_unit.id, group_name : group_unit.name });
                            let discharge = await Db.log_discharge.create(1, 'discharge');
                            charge_id = discharge.id;
                        }
                        if(objData.device && Array.isArray(objData.device)){
                            objData.device.forEach(async unit_data => {
                                // await InsertLogUnit({ 
                                //     group_id : group_unit.id,
                                //     group_name : group_unit.name,
                                //     unit_id: unit_data.idx,
                                //     unit_name: unit_data.idx,
                                //     momh: unit_data.U,
                                //     vol: unit_data.R,
                                //     temp : unit_data.T
                                // });
                                // await InsertLogAlarm({ 
                                //     unit_id: unit_data.idx,
                                //     unit_name: unit_data.idx,
                                //     res: unit_data.U,
                                //     vol: unit_data.R,
                                //     temp : unit_data.T,
                                //     volmin : alarmConfig.volmin,
                                //     volmax : alarmConfig.volmin,
                                //     tempmin : alarmConfig.tempmin,
                                //     tempmax : alarmConfig.tempmax,
                                //     resmin : alarmConfig.resmin,
                                //     resmax : alarmConfig.resmax
                                // });
                                console.log("unit_data : " + unit_data.idx);
                                await Db.log_unit.create(
                                	unit_data.idx,current_ampe,
                                	unit_data.U/1000,0,lastCharge.id,
                                	unit_data.R/10,0,getValueLevel(unit_data.R),unit_data.T);
                                    let status = checkIdWarning(unit_data.U/1000,unit_data.T,unit_data.R/10,
                                	alarmConfig.volmin,alarmConfig.volmax,
                                	alarmConfig.tempmin,alarmConfig.tempmax,
                                	alarmConfig.resmin,alarmConfig.resmax);
                                console.log("status : " + status);
                                if(status){
                                	await Db.logalarm.create(
	                                	unit_data.idx,1,
	                                	status,unit_data.U/1000,unit_data.R/10,1);
                                }
                                
                            });
                        }
                        else{
                            console.log(message.toString());
                            console.log("not device");
                        }
                    }
                    else {//(lastCharge && lastCharge.state === 'discharge'){
                    	console.log("discharge");
                        if(current_ampe <= 0){
                            // await UpdateLogCharge({
                            //     id : lastCharge.id,
                            //     create_datetime : lastCharge.create_datetime,
                            //     status: 'done'
                            // })
                            await Db.log_discharge.update(lastCharge.id,'charge');
                        }
                        if(objData.device && Array.isArray(objData.device)){
                            objData.device.forEach(async unit_data => {
                                // await InsertLogUnit({ 
                                //     group_id : group_unit.id,
                                //     group_name : group_unit.name,
                                //     unit_id: unit_data.idx,
                                //     unit_name: unit_data.idx,
                                //     momh: unit_data.U,
                                //     vol: unit_data.R,
                                //     temp : unit_data.T
                                // });
                                // await InsertLogAlarm({ 
                                //     unit_id: unit_data.idx,
                                //     unit_name: unit_data.idx,
                                //     res: unit_data.U,
                                //     vol: unit_data.R,
                                //     temp : unit_data.T,
                                //     volmin : alarmConfig.volmin,
                                //     volmax : alarmConfig.volmax,
                                //     tempmin : alarmConfig.tempmin,
                                //     tempmax : alarmConfig.tempmax,
                                //     resmin : alarmConfig.resmin,
                                //     resmax : alarmConfig.resmax
                                // });
                                await Db.log_unit.create(
                                	unit_data.idx,current_ampe,
                                	unit_data.U/1000,0,lastCharge.id,
                                	unit_data.R/10,0,getValueLevel(unit_data.R),unit_data.T);
                                // let status = checkIdWarning(unit_data.R,unit_data.U,unit_data.T,
                                // 	alarmConfig.volmin,alarmConfig.volmax,
                                // 	alarmConfig.tempmin,alarmConfig.tempmax,
                                //     alarmConfig.resmin,alarmConfig.resmax);
                                //     await Db.log_unit.create(
                                //         unit_data.idx,current_ampe,
                                //         unit_data.U,0,getValueLevel(unit_data.R),unit_data.T),
                                //         unit_data.R,0,charge_id;
                                    let status = checkIdWarning(unit_data.U/1000,unit_data.T,unit_data.R/10,
                                        alarmConfig.volmin,alarmConfig.volmax,
                                        alarmConfig.tempmin,alarmConfig.tempmax,
                                        alarmConfig.resmin,alarmConfig.resmax);
                                if(status){
                                	await Db.logalarm.create(
	                                	unit_data.idx,1,
	                                	status,unit_data.U/1000,unit_data.R/10,1);
                                }
                            });
                        }
                        else{
                            console.log(message.toString());
                            console.log("not device");
                        }
                    }
                    
                }
                else{
                    console.log(message.toString());
                }
            }
            else{
            	console.log("objData");
                console.log(message.toString());
            }
        }
        catch (ex){
        	console.log("error");
            console.log(ex);
        }
    });

    mqttClient.on('close', () => {
      console.log(`mqtt client disconnected`);
    });
};

function getValueLevel (volBatt) {
    let value = Math.round((volBatt - 9.6)/0.03);
    if (value > 100)
        value = 100
    if (value < 0)
        value = 0
    return value
}

function checkIdWarning (volcheck,tempcheck,rescheck,volmin,volmax,tempmin,tempmax,resmin,resmax) {
	if (volcheck <= parseFloat(volmin) )
		return 'Vol_limit_min'
	if (volcheck >= parseFloat(volmax) )
		return 'Vol_limit_max'
	if (tempcheck <= parseFloat(tempmin) )
		return 'Temp_limit_min'
	if (tempcheck >= parseFloat(tempmax) )
		return 'Temp_limit_max'
	if (rescheck <= parseFloat(resmin) )
		return 'Res_limit_min'
	if (rescheck >= parseFloat(resmax) )
		return 'Res_limit_max'
	return null
}

